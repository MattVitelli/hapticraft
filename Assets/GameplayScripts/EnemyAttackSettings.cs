﻿using UnityEngine;
using System.Collections;

public class EnemyAttackSettings : MonoBehaviour {

	public float NormalizedAttackDamageTime;
	public float AttackDamage;
	public AnimationClip AttackAnimation;
	public AudioClip[] AttackSounds;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
