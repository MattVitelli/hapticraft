using UnityEngine;
using System.Runtime.InteropServices;
using System.Collections;

// Require a character controller to be attached to the same game object
[RequireComponent (typeof (CharacterMotorC))]

//RequireComponent (CharacterMotor)
[AddComponentMenu("Character/HapticController")]
//@script AddComponentMenu ("Character/FPS Input Controller")



public class HapticController : Haptics {

	public CharacterMotorC cmotor;
		
	// Use this for initialization	
	void Awake ()
	{
		cmotor = GetComponent<CharacterMotorC>();	
	}


	public void Update() {
		SetForceCenter (500.0, 0.0, 0.0, 0.0);
		SetDamping(10);
		this.UpdateH();

		
		Vector3 directionVector = new Vector3(y * 40, 0, -x * 30);
		//Debug.Log (directionVector.magnitude);
		if (directionVector.magnitude > 0.12) {
			if (directionVector.magnitude > 1) {
				directionVector.Normalize();
			}
			cmotor.inputMoveDirection = transform.rotation * directionVector;
		} else {
			cmotor.inputMoveDirection = new Vector3(0, 0, 0);
		}

		if (z < -0.02) {
			cmotor.inputJump = true;
		} else {
			cmotor.inputJump = false;
		}
	}
	
}