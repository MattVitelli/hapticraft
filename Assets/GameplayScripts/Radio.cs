﻿using UnityEngine;
using System.Collections;

public class Radio : MonoBehaviour {
	public AudioClip[] BackgroundMusic;
	public AudioSource CurrentTrack;
	public float MusicVolume = 0.3f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(!CurrentTrack.isPlaying) {
			AudioClip clip = RandomHelper.GetRandomAudioClip(ref BackgroundMusic);
			CurrentTrack.clip = clip;
			CurrentTrack.Play();
			CurrentTrack.volume = MusicVolume;
		}
	}
}
