﻿using UnityEngine;
using System.Collections;

public class WeaponSwitcher : MonoBehaviour {

	public MonoBehaviour[] behaviors;
	public Haptics haptics;
	private int enabled = 0;

	// Use this for initialization
	void Start () {
		swapTo(enabled);	
	}

	private void swapTo(int n) {
		for (int i = 0; i < behaviors.Length; i++) {
			behaviors[i].enabled = (i == n);
		}
		enabled = n;
	}

	private bool hit = false;
	
	// Update is called once per frame
	void Update () {
		if (hit) {
			hit = haptics.buttons.left || haptics.buttons.right;
		} else if (haptics.buttons.left) {
			enabled--;
			if (enabled < 0) {
				enabled = this.behaviors.Length - 1; 
			}
			swapTo (enabled);
			hit = true;
		} else if (haptics.buttons.right) {
			enabled++;
			if (enabled >= this.behaviors.Length) {
				enabled = 0;
			}
			swapTo(enabled);
			hit = true;
		}
	}
}
