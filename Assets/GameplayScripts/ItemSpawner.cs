﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemSpawner : MonoBehaviour {

	bool hasSpawnedItems = false;
	public GameObject[] ItemPrefabs;
	public GameObject ShipPrefab;
	public GameObject player;
	public float MinSpawnSpread = 5.0f;
	public float MaxSpawnSpread = 300.0f;
	public TerrainManager terrainManager;
	public float MaxItemDepth = 30.0f;
	// Use this for initialization
	void Start () {
	
	}

	void HandlePlayerSpawning(GameObject ship) {
		Transform[] transforms = ship.GetComponentsInChildren<Transform>();
		foreach(Transform t in transforms) {
			if(t.gameObject.name == "PLAYER_SPAWN") {
				player.transform.position = t.position;
				return;
			}
		}
	}

	void SpawnItems() {
		List<GameObject> itemsToSpawn = new List<GameObject>();
		itemsToSpawn.AddRange(ItemPrefabs);
		itemsToSpawn.Add(ShipPrefab);

		GameObject spawnedShipPrefab = null;

		List<GameObject> itemsSpawned = new List<GameObject>();

		foreach(GameObject obj in itemsToSpawn) {
			bool validHit = false;
			Vector3 spawnPos = Vector3.zero;
			Vector3 normal = Vector3.up;
			while(!validHit) {
				Vector3 randPos = Random.onUnitSphere*MaxSpawnSpread + Vector3.zero;
				validHit = PlacementHelper.DidHitGeometry(randPos, out spawnPos, out normal);
				if(validHit) {
					foreach(GameObject otherItems in itemsSpawned) {
						if((spawnPos-otherItems.transform.position).magnitude < MinSpawnSpread) {
							validHit = false;
						}
					}
				}
			}

			GameObject spawnedObj = (GameObject)Instantiate(obj);
			Collider collider = spawnedObj.GetComponent<Collider>();
			Vector3 offset = Vector3.up*1.5f;
			if(collider != null) {
				offset = Vector3.up*collider.bounds.extents.y;
			}
			spawnedObj.transform.position = spawnPos + offset;
			itemsSpawned.Add(spawnedObj);
			if(obj == ShipPrefab) {
				spawnedShipPrefab = spawnedObj;
				spawnedShipPrefab.transform.up = normal;
			}
			else {
				Vector3 oldPos = spawnedObj.transform.position;
				spawnedObj.transform.position = oldPos + Vector3.down*Random.value*MaxItemDepth;
			}
		}

		HandlePlayerSpawning(spawnedShipPrefab);
	}

	// Update is called once per frame
	void Update () {
		if(!hasSpawnedItems && terrainManager.HasGeneratedSufficientChunks()) {
			hasSpawnedItems = true;
			SpawnItems();
		}
	}
}
