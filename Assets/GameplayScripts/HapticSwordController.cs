﻿using UnityEngine;
using System.Collections;

public class HapticSwordController : MonoBehaviour {
	public Transform Sword;
	public float maxHorizontalAngle = 30.0f;
	public float minHorizontalAngle = -30.0f;
	public float maxVerticalAngle = 45.0f;
	public float minVerticalAngle = -45.0f;
	public float minThrustPos = -0.3f;
	public float maxThrustPos = 0.3f;

	public Haptics haptics;

	bool wasInContact = false;
	Vector3 defaultRotation;
	Vector3 defaultPosition;
	public SwordDamageScript damageScript;
	Vector3 oldSwordLocalPos = Vector3.zero;
	Vector3 oldHapticDevicePos = Vector3.zero;

	//ATP
	bool forceNoCollision = false;

	void SetSwordEnabled(bool enabled) {
		MeshRenderer[] meshes = Sword.GetComponentsInChildren<MeshRenderer>();
		Collider[] colliders = Sword.GetComponentsInChildren<Collider>();
		
		foreach(MeshRenderer mr in meshes) {
			mr.enabled = enabled;
		}
		foreach(Collider c in colliders) {
			c.enabled = enabled;
		}

		//ATP
		damageScript.enabled = enabled;
	}


	// Use this for initialization
	void Start () {
		defaultRotation = Sword.localEulerAngles;
		defaultPosition = Sword.localPosition;
		SetSwordEnabled(true);
		forceNoCollision = true;
	}

	void OnEnable() {
		SetSwordEnabled(true);
		forceNoCollision = true;
	}

	void OnDisable () {
		SetSwordEnabled(false);
	}

	// Update is called once per frame
	void Update () {
		Vector3 hapticDevicePosition = haptics.GetHapticPosition() / Haptics.HAPTIC_WORKSPACE;
		Vector3 localAngles = Sword.localEulerAngles;
		Vector3 localPos = Sword.localPosition;
		float angleX = 0;
		float angleY = 0;
		float dispZ = 0;
		if(hapticDevicePosition.x < 0)
			angleX = Mathf.Lerp(defaultRotation.x, minHorizontalAngle, Mathf.Clamp01(Mathf.Abs(hapticDevicePosition.x)));
		else
			angleX = Mathf.Lerp(defaultRotation.x, maxHorizontalAngle, Mathf.Clamp01(Mathf.Abs(hapticDevicePosition.x)));
		if(hapticDevicePosition.y < 0)
			angleY = Mathf.Lerp(defaultRotation.y, minVerticalAngle, Mathf.Clamp01(Mathf.Abs(hapticDevicePosition.y)));
		else
			angleY = Mathf.Lerp(defaultRotation.y, maxVerticalAngle, Mathf.Clamp01(Mathf.Abs(hapticDevicePosition.y)));
		if(hapticDevicePosition.z < 0)
			dispZ = Mathf.Lerp(defaultPosition.z, minThrustPos, Mathf.Clamp01(Mathf.Abs(hapticDevicePosition.z)));
		else
			dispZ = Mathf.Lerp(defaultPosition.z, maxThrustPos, Mathf.Clamp01(Mathf.Abs(hapticDevicePosition.z)));
		localAngles.x = -angleY;
		localAngles.y = angleX;
		localPos.z = dispZ;
		Sword.localPosition = localPos;
		Sword.localEulerAngles = localAngles;
		damageScript.velocity = (hapticDevicePosition - oldHapticDevicePos) / Time.deltaTime;
		//ATP: && !forceNoCollision
		if(damageScript.isCollision && !forceNoCollision) {
			//SetForceCenter(1000, (damageScript.penetrationDist)*damageScript.collisionNormal + GetHapticPosition());
			//SetForceCenter(2000, GetHapticPosition());
			if(damageScript.hasNotPierced) {
				haptics.SetForceCenter(1000, oldSwordLocalPos);
				//Apply impulse for first contact
				//Vector3 dirOfImpulse = oldSwordLocalPos - damageScript.HapticSwordPos;
			} else {
				haptics.SetForceCenter(1000, oldSwordLocalPos);
			}
			haptics.SetDamping(40);
		} else {
			oldSwordLocalPos = haptics.GetHapticPosition();
			haptics.SetForceCenter(0, haptics.GetHapticPosition());
			haptics.SetDamping(10);
			forceNoCollision = false; //ATP
		}
		wasInContact = damageScript.isCollision;
		oldHapticDevicePos = hapticDevicePosition;
	}
}
