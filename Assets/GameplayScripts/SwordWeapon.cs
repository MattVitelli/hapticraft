﻿using UnityEngine;
using System.Collections;

public class SwordWeapon : MonoBehaviour {

	public MouseLook PlayerLookScript;
	public float MaxDamage = 50;
	public float StabDamage = 120;
	public Animator Avatar;
	public AnimationClip RIGHT_LEFT_ANIMATION;
	public AnimationClip UP_DOWN_ANIMATION;
	public float MaxSwordAngleY = Mathf.PI/2.0f - 0.05f;
	public float MaxSwordAngleX = Mathf.PI/2.0f;
	public Transform Sword;
	public Transform SwordAvatarPos;
	Vector3 desiredPos = Vector3.forward;
	int HORIZONTAL_SWORD_LAYER = 2;
	int VERTICAL_SWORD_LAYER = 3;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

		bool isMouseDown = Input.GetMouseButton(0);
		PlayerLookScript.enabled = !isMouseDown;
		Vector2 mousePos01 = new Vector2(Input.mousePosition.x / Screen.width, Input.mousePosition.y / Screen.height);
		mousePos01.x = Mathf.Clamp01(mousePos01.x);
		mousePos01.y = Mathf.Clamp01(mousePos01.y);
		float xAngle = (mousePos01.x*2.0f-1.0f)*MaxSwordAngleX;
		float yAngle = ((1.0f-mousePos01.y)*2.0f-1.0f)*MaxSwordAngleY;
		float sinPhi = Mathf.Sin(yAngle);
		if(isMouseDown) {
		desiredPos = new Vector3(Mathf.Sin(xAngle)*sinPhi,
		                     	 Mathf.Cos(yAngle),
			                     Mathf.Cos(xAngle)*sinPhi)*2.3f;

		

			Avatar.Play(UP_DOWN_ANIMATION.name, VERTICAL_SWORD_LAYER, mousePos01.y);
			Avatar.Play(RIGHT_LEFT_ANIMATION.name, HORIZONTAL_SWORD_LAYER, 1.0f-mousePos01.x);
			SwordAvatarPos.position = this.transform.TransformPoint(desiredPos);
			float weight = Mathf.Pow(desiredPos.y,2.0f);
			Avatar.SetLayerWeight(HORIZONTAL_SWORD_LAYER, 1.0f-weight);//1.0f-Mathf.Abs(desiredPos.y));
			Avatar.SetLayerWeight(VERTICAL_SWORD_LAYER, weight);//Mathf.Abs(desiredPos.y));
		}
		else {
			Avatar.SetLayerWeight(HORIZONTAL_SWORD_LAYER, 0.0f);//1.0f-Mathf.Abs(desiredPos.y));
			Avatar.SetLayerWeight(VERTICAL_SWORD_LAYER, 0.0f);//Mathf.Abs(desiredPos.y));
		}
	}
	/*
	void OnAnimatorIK(int layerIndex) {
		if(layerIndex == 1) {
			AvatarIKGoal goal = AvatarIKGoal.RightHand;
			Avatar.SetIKPositionWeight(goal, 1.0f);
			Avatar.SetIKRotationWeight(goal, 1.0f);
			Avatar.SetIKPosition(goal,  SwordAvatarPos.transform.position);
			Avatar.SetIKRotation(goal,  SwordAvatarPos.transform.rotation);
			Debug.Log("Wrote IK");
		}

	}*/
}
