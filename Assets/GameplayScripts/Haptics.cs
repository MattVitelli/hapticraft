using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System.Reflection;
using System.IO;
using System.Net.Sockets;
using System.Diagnostics;

public class Haptics : MonoBehaviour {

	public static float HAPTIC_WORKSPACE = 0.08f;
	TcpClient tcpClient;
	NetworkStream networkStream;
	StreamReader clientStreamReader;
	StreamWriter clientStreamWriter;
	public Transform cameraTransform;
	string content = "";
	public bool useDefaultDevice = true;
	public float sensitivityX = 15F;
	public float sensitivityY = 150F;

	void startConn() {
		int port = (useDefaultDevice ? 27015 : 27016);
		tcpClient = new TcpClient();
		tcpClient.Connect("127.0.0.1", port);
		networkStream = tcpClient.GetStream();
		clientStreamReader = new StreamReader(networkStream);
		clientStreamWriter = new  StreamWriter(networkStream);
	}

	void Start() {
	}

	int state = 0;


	public float x, y, z;
	public Vector3 GetHapticPosition() {
		return new Vector3(y, z, -x);
	}

	public void SetForceCenter(double k, Vector3 unityToolPos) {
		SetForceCenter(k, -unityToolPos.z, unityToolPos.x, unityToolPos.y);
	}

	public void SetDrillRate(double r, Vector3 drillVec) {
		SetDrillRate(r, -drillVec.z, drillVec.x, drillVec.y);
	}

	public struct ButtonStates {
		public bool left;
		public bool right;
		public bool center;
		public bool front;
	};
	public ButtonStates buttons;

	//private float rotationY = 0;

	double attract_k, attract_x, attract_y, attract_z;
	public void SetForceCenter(double k, double x, double y, double z) {
		state = 1;
		attract_k = k;
		attract_x = x;
		attract_y = y;
		attract_z = z;
	}

	public double drill_r = 0, drill_x = 0, drill_y = 0, drill_z = 0;
	public void SetDrillRate(double r, double x, double y, double z) {
		drill_r = r;
	}

	public double damping;
	public void SetDamping(double damping) {
		this.damping = damping;	
	}

	float rotationY = 0;

	public void Update() {
		if (buttons.center) {
			SetDrillRate(0, Vector3.zero);
			SetDamping(10);
			SetForceCenter(200, 0, 0, 0);
		}
		UpdateH();
		//This is where clutchin
		if(buttons.center) {
			UpdateRotations(0.02f, 0.004f);
		}
		/*
		if (true && useDefaultDevice) {
			UpdateRotations(0.05f, 0.045f);
		}
		*/
	}

	void UpdateRotations(float verticalDeadZone, float horizontalDeadZone) {
		float deadZoneHorizontal = (Mathf.Abs(y)-horizontalDeadZone > 0.0f)?1.0f:0.0f;
		float rotationX = transform.localEulerAngles.y + y * sensitivityX * deadZoneHorizontal;
		
		float deadZoneVertical = (Mathf.Abs(z)-verticalDeadZone > 0.0f)?1.0f:0.0f;
		rotationY += -z * sensitivityY * deadZoneVertical;
		//UnityEngine.Debug.Log("Before break " + rotationY);
		rotationY = Mathf.Clamp (rotationY, -60.0f, 60.0f);
		//UnityEngine.Debug.Log("After break " + rotationY);
		
		cameraTransform.localEulerAngles = new Vector3(rotationY, 0, 0);
		transform.localEulerAngles = new Vector3(0, rotationX, 0);
	}

	public void UpdateH() {
		if (networkStream == null) {
			startConn();
			return;
		}

		while (networkStream.DataAvailable) {
			content += (char)clientStreamReader.Read();
		}

		Stopwatch stopwatch = new Stopwatch();
		stopwatch.Start();

		if (state == 0) {
			clientStreamWriter.Write ("YO\n");
		} else if (state == 1) {
			clientStreamWriter.Write ("SETFP," + this.attract_k + "," + this.attract_x
			                          + "," + this.attract_y + "," + this.attract_z + "\n");
		}
		clientStreamWriter.Write("DRILL," + this.drill_r + "," + this.drill_x
		                         + "," + this.drill_y + "," + this.drill_z + "\n");
		clientStreamWriter.Write("DAMPING," + this.damping + "\n");
		clientStreamWriter.Flush ();
		content += (char)clientStreamReader.Read(); //Expect atleast one data
		while (networkStream.DataAvailable) {
			content += (char)clientStreamReader.Read();
		}
		stopwatch.Stop();
		//UnityEngine.Debug.Log (stopwatch.ElapsedMilliseconds + " ms");
		while (content.Contains("\n")) {
			string packet = content.Split('\n')[0].Trim();
			content = content.Substring(content.IndexOf("\n") + 1);
			//UnityEngine.Debug.Log (packet);
			string[] packetsplit = packet.Split(',');
			if (packetsplit.Length < 1) {
			} else if (packetsplit[0].Equals("POS")) {
				x = float.Parse(packetsplit[1]);
				y = float.Parse(packetsplit[2]);
				z = float.Parse(packetsplit[3]);
				buttons.center = packetsplit[4][0] == 'T' ? true : false;
				//buttons.center = packetsplit[4][0] == 'T' ? false : true;

				buttons.left = packetsplit[4][1] == 'T' ? true : false;
				buttons.front = packetsplit[4][2] == 'T' ? true : false;
				buttons.right = packetsplit[4][3] == 'T' ? true : false;
			}
		}
	}


	/*const string falcon = "FalconWrapper";
	
	[DllImport(falcon)]
	private static extern void StartHaptics();
	[DllImport(falcon)]
	private static extern void StopHaptics();
	[DllImport(falcon)]
	private static extern bool IsDeviceCalibrated();
	[DllImport(falcon)]
	private static extern bool IsDeviceReady();
	[DllImport(falcon)]
	private static extern double GetXPos();
	[DllImport(falcon)]
	private static extern double GetYPos();
	[DllImport(falcon)]
	private static extern double GetZPos();
	[DllImport(falcon)]
	private static extern void SetServo(double[] speed);
	[DllImport(falcon)]
	private static extern void SetServoPos(double[] pos, double strength);
	[DllImport(falcon)]
	private static extern bool IsHapticButtonDepressed();
	[DllImport(falcon)]
	private static extern int GetButtonsDown();
	[DllImport(falcon)]
	private static extern bool isButton0Down();
	[DllImport(falcon)]
	private static extern bool isButton1Down();
	[DllImport(falcon)]
	private static extern bool isButton2Down();
	[DllImport(falcon)]
	private static extern bool isButton3Down();
	
	public static Haptics main;
	
	
	// Use this for initialization
	void Start () 
	{
		StartHaptics();
		StartCoroutine(InitHaptics());
	}
	
	private IEnumerator InitHaptics()
	{
		while(!IsDeviceCalibrated())
		{
			Debug.LogWarning("Please calibrate the device!");
			yield return new WaitForSeconds(1.5f);
		}
		if(!IsDeviceReady())
			Debug.LogError("Device is not ready!");
		
		main = this;
	}
	
	void OnApplicationQuit () 
	{
		StopHaptics();
	}
	
	public Vector3 GetServoPos()
	{
		return new Vector3((float)GetXPos(), (float)GetYPos(), -(float)GetZPos() );
	}
	
	public void SetServo(Vector3 speed)
	{
		double[] _speed = new double[3];
		_speed[0] = speed.x;
		_speed[1] = speed.y;
		_speed[2] = speed.z;
		SetServo(_speed);
	}
	
	internal void SetServoPos(Vector3 pos, double strength)
	{
		double[] _pos = new double[3];
		_pos[0] = pos.x;
		_pos[1] = pos.y;
		_pos[2] = pos.z;
		SetServoPos(_pos, strength);
	}
	
	void Update ()
	{
		//SetServo(new double[3]{0,0,-10});
		//SetServoPos(new double[3]{ 0, 0, 0 }, 3.0 );
		
		//gameObject.transform.position = new Vector3( (float)GetXPos()*-2, (float)GetYPos()*2, (float)GetZPos()*2 );
		
		Debug.Log ( isButton0Down() + " , " + isButton1Down() + " , " + isButton2Down() + " , " + isButton3Down() );
		
	}*/


	
}