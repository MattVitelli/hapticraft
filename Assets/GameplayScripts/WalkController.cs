﻿using UnityEngine;
using System.Collections;

public class WalkController : MonoBehaviour {
	
	public float walkSpreadRight = 5;
	public float walkSpreadForward = 5;
	public float walkPeriod = 10;
	public float stepMagnitude = 1.2f;
	public float upperRightShift = 0.0f;
	public float lowerRightShift = 1.0f;
	public float upperLeftShift = 2.0f;
	public float lowerLeftShift = 3.0f;
	public float forceMagnitude = 1.5f;
	public Rigidbody upperLeftLimb;
	public Rigidbody upperRightLimb;
	public Rigidbody lowerLeftLimb;
	public Rigidbody lowerRightLimb;
	public Rigidbody root;
	Vector3[] oldLimbPoses = new Vector3[4];
	Vector3[] newLimbPoses = new Vector3[4];
	float[] elapsedTimes = new float[4];
	Rigidbody[] limbs = new Rigidbody[4];
	// Use this for initialization
	void Start () {
		limbs[0] = upperLeftLimb;
		limbs[1] = upperRightLimb;
		limbs[2] = lowerLeftLimb;
		limbs[3] = lowerRightLimb;
		oldLimbPoses[0] = upperLeftLimb.transform.position; elapsedTimes[0] = upperLeftShift/walkPeriod;
		oldLimbPoses[1] = upperRightLimb.transform.position;elapsedTimes[1] = upperRightShift/walkPeriod;
		oldLimbPoses[2] = lowerLeftLimb.transform.position; elapsedTimes[2] = lowerLeftShift/walkPeriod;
		oldLimbPoses[3] = lowerRightLimb.transform.position;elapsedTimes[3] = lowerRightShift/walkPeriod;
		for(int i = 0; i < oldLimbPoses.Length; i++) {
			newLimbPoses[i] = oldLimbPoses[i];
		}
	}

	void updateLimbAnimation(int limbIndex, float period, float shift, Vector3 newPos) {
		/*
		Ray rD = new Ray(newPos, Vector3.down);
		Ray rU = new Ray(newPos, Vector3.up);
		RaycastHit hitD;
		RaycastHit hitU;
		bool downHit = Physics.Raycast(rD, out hitD);
		bool upHit = Physics.Raycast(rU, out hitU);
		Ray minDistRay = (downHit && (!upHit || hitD.distance < hitU.distance))? rD : rU;
		RaycastHit minDistRayInfo = (downHit && (!upHit || hitD.distance < hitU.distance))? hitD : hitU;
		if(upHit || downHit) {*/
		{
			Rigidbody limb = limbs[limbIndex];

			//Vector3 oldLocalPos = limb.transform.localPosition;
			elapsedTimes[limbIndex] += Time.deltaTime;
			float interp = Mathf.Min(1.0f, elapsedTimes[limbIndex] / period);
			//Mathf.Sin(Time.time*period + shift*Mathf.PI*2.0f/period)*0.5f+0.5f;
			Vector3 pos = Vector3.Lerp(oldLimbPoses[limbIndex], newLimbPoses[limbIndex], interp);
			if(interp < 0.5f) {
				pos.y = Mathf.Lerp(oldLimbPoses[limbIndex].y, oldLimbPoses[limbIndex].y + stepMagnitude, interp / 0.5f);
			} else {
				pos.y = Mathf.Lerp(oldLimbPoses[limbIndex].y + stepMagnitude, oldLimbPoses[limbIndex].y, (interp-0.5f) / 0.5f);
			}
			limb.transform.position = pos;
			if(elapsedTimes[limbIndex] >= period) {
				oldLimbPoses[limbIndex] = newLimbPoses[limbIndex];//limb.transform.localPosition;
				elapsedTimes[limbIndex] = 0.0f;

				Ray rD = new Ray(newPos, Vector3.down);
				Ray rU = new Ray(newPos, Vector3.up);
				RaycastHit hitD;
				RaycastHit hitU;
				bool downHit = Physics.Raycast(rD, out hitD);
				bool upHit = Physics.Raycast(rU, out hitU);
				Ray minDistRay = (downHit && (!upHit || hitD.distance < hitU.distance))? rD : rU;
				RaycastHit minDistRayInfo = (downHit && (!upHit || hitD.distance < hitU.distance))? hitD : hitU;
				if(upHit || downHit) {
					Vector3 ptIntersectWorldPos = minDistRay.origin + minDistRay.direction * minDistRayInfo.distance;
					Vector3 ptLocalPos = limb.transform.InverseTransformPoint(ptIntersectWorldPos);
					newLimbPoses[limbIndex] = ptIntersectWorldPos;//ptLocalPos;
				}
			}
			/*
			float limbMass = limb.mass;
			float mag = forceMagnitude*Mathf.Sin(Time.time*period + shift*Mathf.PI*2.0f/period);
			Vector3 force = -forceMagnitude*(Mathf.Sin(Time.time*period + shift*Mathf.PI*2.0f/period)*0.5f+0.5f)*limbMass*Physics.gravity;
			Vector3 localPos = limb.transform.localPosition;
			localPos.y = mag;
			limb.transform.localPosition = localPos;
			*/
		}
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 curPoint = root.transform.position;
		Vector3 headingDir = root.transform.forward;
		Vector3 rightDir = root.transform.right;
		Vector3 predictedDisp = headingDir*walkSpreadForward;
		//predictedDisp.Scale(root.velocity);
		Vector3 nextPos = curPoint + predictedDisp;
		Vector3 nextUpperRight = nextPos + rightDir*walkSpreadRight + headingDir*walkSpreadForward;
		Vector3 nextUpperLeft = nextPos - rightDir*walkSpreadRight + headingDir*walkSpreadForward;
		Vector3 nextLowerRight = nextPos + rightDir*walkSpreadRight - headingDir*walkSpreadForward;
		Vector3 nextLowerLeft = nextPos - rightDir*walkSpreadRight - headingDir*walkSpreadForward;
		updateLimbAnimation(0, walkPeriod, upperLeftShift, nextUpperLeft);
		updateLimbAnimation(1, walkPeriod, upperRightShift, nextUpperRight);
		updateLimbAnimation(2, walkPeriod, lowerLeftShift, nextLowerLeft);
		updateLimbAnimation(3, walkPeriod, lowerRightShift, nextLowerRight);
	}
}
