﻿using UnityEngine;
using System.Collections;

public class Perlin3D : MonoBehaviour {

	public Texture2D testTexture;
	Camera renderCamera;
	Mesh quad;
	public Material testMat;
	public Material proceduralMat;

	public class Permutation {
		int[] m_perm;
		int m_B;
		public Texture2D m_permTex;
		public float amplitude;
		public float persistance;
		public float frequency;
		public int numOctaves;
		public Permutation(int b, int seed, float freq, float amp, float persist, int numOcts) {
			m_B = b;
			m_perm = new int[m_B*m_B];
			GeneratePermutations(seed);
			LoadPermTableIntoTexture();
			amplitude = amp;
			frequency = freq;
			persistance = persist;
			numOctaves = numOcts;
		}

		void GeneratePermutations(int seed) {
			System.Random rand = new System.Random(seed);
			for(int j = 0; j < m_B; j++) {
				for(int i = 0; i < m_B; i++) {
					m_perm[i+j*m_B] = rand.Next(0,255);
				}
			}
		}

		public void LoadPermTableIntoTexture()
		{
			m_permTex = new Texture2D(m_B, m_B, TextureFormat.ARGB32, false);
			m_permTex.filterMode = FilterMode.Bilinear;
			m_permTex.wrapMode = TextureWrapMode.Repeat;
			
			for(int j = 0; j < m_B; j++) {
				for(int i = 0; i < m_B; i++) {
					float v = (float)m_perm[i+j*m_B] / 255.0f;
					
					m_permTex.SetPixel(i, j, new Color(v,v,v,v));
				}
			}
			
			m_permTex.Apply();
		}
	}

	const int randomSize = 256;
	const int seed = 13563;
	
	public Shader m_shader;
	public RenderTexture m_renderTexture;
	Permutation[] m_permutations;
	
	// Use this for initialization
	void Start () {
		m_renderTexture = new RenderTexture(256, 256, 1, RenderTextureFormat.ARGB32);
		testTexture = new Texture2D(256, 256, TextureFormat.ARGB32, false);
		bool ret = m_renderTexture.Create();
		Debug.Log("RenderTexture creation was " + ret);
		m_permutations = new Permutation[2];

		m_permutations[0] = new Permutation(randomSize, seed, 0.018499f, 0.00249f, 0.70999f, 16);
		m_permutations[1] = new Permutation(randomSize, seed, 0.0018499f, 0.29f, 0.90999f, 16);
		//RenderIntoTexture(m_shader, m_renderTexture, m_permutations[0]);
		RenderIntoTexture(m_shader, m_renderTexture, testTexture, m_permutations[1]);
		testMat.mainTexture = m_renderTexture;
		testMat.mainTexture = testTexture;
		//testMat.mainTexture = m_permutations[0].m_permTex;
		/*
		RenderTexture oldTex = RenderTexture.active;
		testTexture = new Texture2D(width, height, TextureFormat.Alpha8, false);
		RenderTexture renderTexture = new RenderTexture(width, height, 1, RenderTextureFormat.R8, RenderTextureReadWrite.Default);
		renderCamera = GetComponent<Camera>();
		renderCamera.targetTexture = renderTexture;
		Graphics.DrawMesh(quad, Matrix4x4.identity, proceduralMat, 0, renderCamera);
		RenderTexture.active = renderTexture;

		Rect rectReadPicture = new Rect(0,0, width, height);
		testTexture.ReadPixels(rectReadPicture, 0, 0);
		activeCamera.targetTexture = null;
		renderCamera.targetTexture = null;
		RenderTexture.active = oldTex; // added to avoid errors 
		DestroyImmediate(renderTexture);
		testMat.mainTexture = testTexture;
		*/
	}


	
	public void RenderIntoTexture(Shader shader, RenderTexture renderTex, Texture2D outTexture, Permutation perm) {

		Material mat = new Material(shader);
		//Frequency, amplitude, persistance, octaves
		float freq = perm.frequency;
		float amplitude = perm.amplitude;
		float persistance = perm.persistance;
		Texture2D tex = perm.m_permTex;
		mat.SetVector("_InvTexSize", new Vector4(1.0f/(float)tex.width, 1.0f / (float)tex.height, 0, 0));
		mat.SetTexture("_NoiseTex", tex);
		for(int i = 0; i < perm.numOctaves; i++) {
			Vector4 noiseVec = new Vector4(freq, amplitude, persistance, 0);
			mat.SetVector("_NoiseParams", noiseVec);
			Graphics.Blit(null, renderTex, mat);
			freq *= 1.995f;
			amplitude *= persistance;
		}
		RenderTexture.active = renderTex;
		outTexture.ReadPixels(new Rect(0, 0, renderTex.width, renderTex.height), 0, 0);
		outTexture.Apply();
		RenderTexture.active = null;
	}

	// Update is called once per frame
	void Update () {

	}
}
