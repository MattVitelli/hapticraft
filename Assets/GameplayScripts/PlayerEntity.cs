﻿using UnityEngine;
using System.Collections;

public class PlayerEntity : MonoBehaviour {
	public float Health = 100;
	public float MaxHealth = 100;
	public Team team = Team.PLAYER;

	public AudioClip[] OnDamagedSources;
	public AudioClip[] OnDeathSources;

	public float TimeDead = 0.0f;

	public float TimeTilPlayerRespawn = 6.0f;

	public float MaxRespawnDistance = 200;

	public bool IsDead {
		get { return Health <= 0; }
	}

	float timeSinceLastHit = 0.0f;
	public float TimeBeforeHeal = 15.0f;
	public float SecondsTilFullHeal = 20.0f;

	public float timeTilNextSound = 0.0f;

	public enum Team {
		PLAYER,
		ZOMBIE,
		ALIEN
	};

	public float GetHealthPercentage() {
		return Health / MaxHealth;
	}

	public void ApplyDamage(float damage) {
		bool wasDead = IsDead;
		if(wasDead)
			return;
		timeSinceLastHit = 0.0f;
		Health -= damage;
		AudioClip clip = null;
		if(IsDead) {
			clip = RandomHelper.GetRandomAudioClip(ref OnDeathSources);
			if(team == Team.PLAYER) {
				OnPlayerDeath();
			}
		} else {
			clip = RandomHelper.GetRandomAudioClip(ref OnDamagedSources);
		}

		if(clip != null && timeTilNextSound <= 0.0f) {
			Debug.Log("Playing clip");
			timeTilNextSound = clip.length;
			AudioSource.PlayClipAtPoint(clip, this.transform.position);
		}
	}
	
	// Use this for initialization
	void Start () {
		Revive();
	}

	void OnPlayerDeath() {
		PlayerGUI gui = FindObjectOfType<PlayerGUI>();
		gui.SetDeathState(true);
	}

	void OnPlayerRevive() {
		PlayerGUI gui = FindObjectOfType<PlayerGUI>();
		gui.SetDeathState(false);
		CharacterController charControl = this.gameObject.GetComponent<CharacterController>();
		Vector3 playerDeathPos = transform.position;
		Vector3 spawnPos = playerDeathPos;
		bool validHit = false;
		while(!validHit) {
			Vector3 randPos = Random.onUnitSphere*MaxRespawnDistance + playerDeathPos;
			validHit = PlacementHelper.DidHitGeometry(randPos, out spawnPos);
		}
		transform.position = spawnPos + Vector3.up * charControl.height;
	}

	public void Revive() {
		Health = MaxHealth;
		if(team == Team.PLAYER) {
			OnPlayerRevive();
		}
	}
	
	// Update is called once per frame
	void Update () {
		timeTilNextSound = Mathf.Max(0.0f, timeTilNextSound-Time.deltaTime);
		timeSinceLastHit += Time.deltaTime;
		if(timeSinceLastHit >= TimeBeforeHeal && team == Team.PLAYER) {
			Health = Mathf.Min(MaxHealth, Health + Time.deltaTime*(MaxHealth / SecondsTilFullHeal));
		}

		if(IsDead) {
			TimeDead += Time.deltaTime;
			if(team == Team.PLAYER) {
				PlayerGUI gui = FindObjectOfType<PlayerGUI>();
				gui.SetDeathTimeTilRespawn(Mathf.Max(0.0f, TimeTilPlayerRespawn-TimeDead));
				if(TimeDead >= TimeTilPlayerRespawn) {
					Revive();
				}
			}
		}

	}
}
