using UnityEngine;
using System.Runtime.InteropServices;
using System.Collections;
using System.Collections.Generic;

public class HapticVoxelController : MonoBehaviour {
	public float stiffness = 0.0f;//1000.0f;
	public float hapticWorkspace = 0.05f;
	public float worldWorkspace = 30.0f;
	public float drillRate = 1000.0f;
	public float DigThreshold = 0.5f;
	public float drillTerrainRadius = 2.0f;
	public float spaceDamping = 20.0f;
	public Transform hapticAvatar;
	public Transform drillAvatar;
	public Transform characterAvatar;

	public Camera playerCamera;
	public CharacterMotorC motor;
	public HapticController motionController;
	public Haptics haptics;

	List<TerrainGenerator> generatorsToUpdate = new List<TerrainGenerator>();
	// Use this for initialization	
	void OnEnable() {
		SetGameObjectRenderingEnabled(hapticAvatar.gameObject, true);
		SetGameObjectRenderingEnabled(drillAvatar.gameObject, true);
		drillAvatar.transform.position = hapticAvatar.transform.position;
	}
	
	void OnDisable () {
		SetGameObjectRenderingEnabled(hapticAvatar.gameObject, false);
		SetGameObjectRenderingEnabled(drillAvatar.gameObject, false);
		motionController.enabled = true;
	}

	void SetGameObjectRenderingEnabled(GameObject obj, bool enabled) {
		MeshRenderer[] meshes = obj.GetComponentsInChildren<MeshRenderer>();
		foreach(MeshRenderer mr in meshes) {
			mr.enabled = enabled;
		}
	}


	public void Update() {
		if(haptics.buttons.center || !motor.isGrounded()) {
			drillAvatar.transform.position = hapticAvatar.transform.position;
			SetGameObjectRenderingEnabled(hapticAvatar.gameObject, false);
			SetGameObjectRenderingEnabled(drillAvatar.gameObject, false);
			return;
		}
		else {
			SetGameObjectRenderingEnabled(hapticAvatar.gameObject, true);
			SetGameObjectRenderingEnabled(drillAvatar.gameObject, true);
		}
	
		Vector3 hapticDevicePosition = haptics.GetHapticPosition();
		Vector3 camFwd = this.transform.InverseTransformDirection(playerCamera.transform.forward);
		Vector3 pos = hapticDevicePosition*worldWorkspace/hapticWorkspace + camFwd*worldWorkspace*1.1f;
		hapticAvatar.transform.localPosition = pos;

		Vector3 hapticToolPos = hapticAvatar.transform.position;
		Vector3 drillPos = drillAvatar.transform.position;
		Vector3 desiredDir = hapticToolPos - drillPos;
		float maxDist = desiredDir.magnitude;
		desiredDir.Normalize();

		Ray ray = new Ray(drillPos, desiredDir);//camera.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
		//Ray ray = new Ray(camera.transform.position, camera.transform.forward);
		RaycastHit info;
		double stiff = 0;
		if(Physics.Raycast(ray, out info, maxDist)) {
			drillAvatar.transform.position = (info.distance-0.01f)*ray.direction + ray.origin;
			haptics.SetDrillRate(drillRate, Vector3.zero);

			float distToTool = (drillAvatar.transform.position - hapticToolPos).magnitude;
			if(distToTool > DigThreshold && GetTerrainGeneratorsToUpdate(info.transform)) {

				float radius = drillTerrainRadius*distToTool;
				//Debug.Log("Beginning Erosion");
				foreach(TerrainGenerator chunk in generatorsToUpdate) {
					chunk.ModifyDensityField(info.point, radius, true, false, 0);
				}
			}
			stiff = stiffness;
			motionController.enabled = false;
			motor.inputMoveDirection = Vector3.zero;
		} else {
			drillAvatar.transform.position = hapticToolPos;
			haptics.SetDrillRate(0, Vector3.zero);
			stiff = 0;
			motionController.enabled = true;
		}
		Vector3 drillPosCameraSpace = characterAvatar.InverseTransformPoint(drillAvatar.transform.position);

		Vector3 forcePos = (drillPosCameraSpace - pos)*hapticWorkspace/worldWorkspace + hapticDevicePosition;
		haptics.SetForceCenter(stiff, forcePos);

		haptics.SetDamping(spaceDamping);
	}

	bool GetTerrainGeneratorsToUpdate(Transform srcTransform) {
		generatorsToUpdate.Clear();
		Transform t = srcTransform;
		TerrainGenerator gen = t.gameObject.GetComponent<TerrainGenerator>();
		while(gen == null && t.parent != null) {
			//Update parent
			t = t.parent;
			//Update generator
			gen = t.gameObject.GetComponent<TerrainGenerator>();
		}
		
		if(gen != null) {
			//Debug.Log("Found object!");
			generatorsToUpdate.Add(gen);
			//If there's a parent, check if parent has terrain manager
			//Otherwise, return null
			TerrainManager mgr = (t.parent != null ? t.parent.gameObject.GetComponent<TerrainManager>() : null);
			if(mgr != null) {
				//Update neighboring voxels
				generatorsToUpdate.AddRange(mgr.GetNeighboringVoxels(gen));
			}
		}
		return generatorsToUpdate.Count > 0;
	}
	
}