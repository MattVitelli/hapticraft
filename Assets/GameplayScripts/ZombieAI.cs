﻿using UnityEngine;
using System.Collections;

public class ZombieAI : MonoBehaviour {

	public Transform eyeTransform;

	public float VisionMaxDistance = 50.0f;
	public float VisionMaxAngleDegrees = 30;
	public float MaxSpeed = 10;
	public float MaxTurnSpeed = 0.5f;
	public float MinAttackDistance = 0.9f;
	bool hasTarget = false;
	bool hasAppliedDamageToTarget = false;
	public int AttackAnimationLayerIndex = 1;
	GameObject target;

	public EnemyAttackSettings[] AttackSettings;
	public AudioClip[] IdleSounds;
	public AudioClip[] ChaseSounds;
	public AudioClip[] TongueSounds;

	float elapsedWanderTime = float.PositiveInfinity;
	float TimeTilNextWander = 45.0f;
	bool isWandering = false;
	Vector3 wanderPos = Vector3.zero;
	public float MaxWanderDistance = 100.0f;

	float timeSinceLastAudioSource = 0.0f;
	const float TimeToPickNextAudioSource = 8.0f;
	public AudioSource characterSound;

	public Transform RagdollRoot;
	bool enabledRagdoll = false;

	public float TooCloseToEnemyDist = 2.1f;

	Animator animController;
	CharacterController charCtrl;
	PlayerEntity entSelf;
	public int DefaultLayer = 0;

	enum ZombieAIMode {
		IDLE,
		CHASE_PLAYER,
		TONGUE_PLAYER,
		DEAD
	};

	ZombieAIMode currentMode = ZombieAIMode.IDLE;
	public PlayerEntity player;

	void setRagdoll(bool enabled) {
		if(RagdollRoot == null)
			return;

		Collider[] colliders = RagdollRoot.gameObject.GetComponentsInChildren<Collider>();
		Rigidbody[] bodies = RagdollRoot.gameObject.GetComponentsInChildren<Rigidbody>();
		foreach(Rigidbody b in bodies) {
			if(enabled) {
				b.useGravity = true;
				b.constraints = RigidbodyConstraints.None;
				b.WakeUp();
			} else {
				b.useGravity = false;
				b.constraints = RigidbodyConstraints.FreezeAll;
				b.Sleep();
			}
		}
		if(enabled) {
			foreach(Collider c in colliders) {
				c.gameObject.layer = DefaultLayer;
			}
		}
		/*
		Collider mainCollider = this.GetComponent<Collider>();
		if(mainCollider != null) {
			mainCollider.enabled = !enabled;
		}
		*/
		CharacterController charCtrl = this.GetComponent<CharacterController>();
		if(charCtrl != null) {
			charCtrl.enabled = !enabled;
		}
		Animator animCtrl = this.GetComponent<Animator>();
		if(animCtrl != null) {
			animCtrl.enabled = !enabled;
		}
	}

	// Use this for initialization
	void Start () {
		setRagdoll(false);
		animController = this.GetComponent<Animator>();
		charCtrl = this.GetComponent<CharacterController>();
		entSelf = this.GetComponent<PlayerEntity>();
		PlayerEntity[] ents = Object.FindObjectsOfType<PlayerEntity>();
		foreach(PlayerEntity ent in ents) {
			if(ent.team == PlayerEntity.Team.PLAYER) {
				player = ent;
			}
		}
	}

	void idleLogic() {
		elapsedWanderTime += Time.deltaTime;

		if(isWandering) {
			Vector3 toEnt = wanderPos - this.transform.position;
			if(toEnt.magnitude <= MinAttackDistance) {
				isWandering = false;
			}
			else {
				float speed = MaxSpeed * Time.deltaTime;
				Vector3 gDir = Physics.gravity;
				gDir.Normalize();
				toEnt = toEnt - Vector3.Dot(gDir,toEnt)*gDir;
				toEnt.Normalize();
				float cosAngle = Vector3.Dot(toEnt, this.transform.forward);
				float deltaAngle = Mathf.Rad2Deg*Mathf.Acos(cosAngle);
				deltaAngle *= ((Vector3.Dot(this.transform.right, toEnt) < 0) ? 1.0f : -1.0f);
				Vector3 rot = this.transform.eulerAngles;
				rot.y = rot.y - deltaAngle*Time.deltaTime*MaxTurnSpeed;
				this.transform.eulerAngles = rot;
				charCtrl.Move(speed*toEnt);
			}
		}
		if(elapsedWanderTime >= TimeTilNextWander) {
			bool anyHit = false;
			while(!anyHit) {
				Vector3 randPos = Random.onUnitSphere*MaxWanderDistance + this.transform.position;
				anyHit = PlacementHelper.DidHitGeometry(randPos, out wanderPos);
			}
			isWandering = true;
			elapsedWanderTime = 0.0f;
		}

		//PlayerEntity[] ents = Object.FindObjectsOfType<PlayerEntity>();
		float visionCutoff = Mathf.Cos(Mathf.Deg2Rad*VisionMaxAngleDegrees);

		//for(int i = 0; i < ents.Length; i++) {
			if(player.team != entSelf.team) {

				Vector3 toEnt = player.transform.position-eyeTransform.position;
				float dist = toEnt.magnitude;
				toEnt /= dist;
				float cosAngle = Vector3.Dot(toEnt, eyeTransform.forward);
				float deltaAngle = Mathf.Acos(cosAngle);
				//Ray ray = new Ray(eyeTransform.position, toEnt);
				//RaycastHit hitInfo;
				//bool anyHit = Physics.Raycast(ray, out hitInfo, VisionMaxDistance);
				//bool entityMatches = (hitInfo.transform == ents[i].transform);
				bool canSeeTarget = (cosAngle > visionCutoff && dist < VisionMaxDistance);
				if(canSeeTarget){
					currentMode = ZombieAIMode.CHASE_PLAYER;
					target = player.gameObject;
					//break;
				}

			}
		//}
		animController.SetFloat("MovementSpeed", charCtrl.velocity.magnitude);
	}

	void chaseLogic() {
		PlayerEntity targetEnt = target.GetComponent<PlayerEntity>();
		PlayerEntity selfEnt = this.GetComponent<PlayerEntity>();
		bool canAttack = false;

		if(target != null && targetEnt != null && !targetEnt.IsDead) {
			Vector3 toEnt = target.transform.position-this.transform.position;
			float distToEnt = toEnt.magnitude;

			Vector3 gDir = Physics.gravity;
			gDir.Normalize();
			toEnt = toEnt - Vector3.Dot(gDir,toEnt)*gDir;
			float distAlongGround = toEnt.magnitude;
			toEnt.Normalize();
			float cosAngle = Vector3.Dot(toEnt, this.transform.forward);
			float deltaAngle = Mathf.Rad2Deg*Mathf.Acos(cosAngle);
			deltaAngle *= ((Vector3.Dot(this.transform.right, toEnt) < 0) ? 1.0f : -1.0f);

			canAttack = (distToEnt < MinAttackDistance*1.25f && distAlongGround < MinAttackDistance);
			float speed = MaxSpeed*Time.deltaTime;
			Vector3 moveDir = toEnt*speed;
			Vector3 rot = this.transform.eulerAngles;
			rot.y = rot.y - deltaAngle*Time.deltaTime*MaxTurnSpeed;
			this.transform.eulerAngles = rot;

			if(!canAttack) {
				/*
				PlayerEntity[] otherEnts = FindObjectsOfType<PlayerEntity>();
				foreach(PlayerEntity otherEnt in otherEnts) {
					if(otherEnt != targetEnt && otherEnt != selfEnt) {
						Vector3 awayFromOtherEnt = transform.position - otherEnt.transform.position;
						float distToOtherEnt = awayFromOtherEnt.magnitude;
						if(distToOtherEnt < MinAttackDistance) {
							awayFromOtherEnt.Normalize();
							moveDir += awayFromOtherEnt*speed;
						}
					}
				}
				*/
				charCtrl.Move(moveDir);
			}
			if(distAlongGround <= TooCloseToEnemyDist) {
				charCtrl.Move(-toEnt*MaxSpeed*Time.deltaTime);
			}
		}
		else {
			currentMode = ZombieAIMode.IDLE;
			target = null;
			hasTarget = false;
			hasAppliedDamageToTarget = false;
		}

		if(canAttack) {
			AnimatorStateInfo animAttackInfo = animController.GetCurrentAnimatorStateInfo(AttackAnimationLayerIndex);
			bool anyValidAttackFound = false;
			foreach(EnemyAttackSettings attack in AttackSettings) {
				//Debug.Log(attack.AttackAnimation.name);
				if(animAttackInfo.IsName(attack.AttackAnimation.name)) {
					anyValidAttackFound = true;
					if(animAttackInfo.normalizedTime >= attack.NormalizedAttackDamageTime && !hasAppliedDamageToTarget) {
						hasAppliedDamageToTarget = true;
						AudioSource.PlayClipAtPoint(RandomHelper.GetRandomAudioClip(ref attack.AttackSounds), this.transform.position);
						targetEnt.ApplyDamage(attack.AttackDamage);
						animController.SetInteger("AttackMode", RandomHelper.GetRandomIntInRange(AttackSettings.Length));
					}
				}
			}
			//If we're in idle mode on the attack layer, then we are free to attack again
			if(!anyValidAttackFound) {
				hasAppliedDamageToTarget = false;
			}
		}

		animController.SetFloat("MovementSpeed", charCtrl.velocity.magnitude);
		animController.SetBool("CanAttack", canAttack && !hasAppliedDamageToTarget);
	}

	void updateAILogic() {

		PlayerEntity ent = this.GetComponent<PlayerEntity>();
		if(ent != null && ent.IsDead) {
			currentMode = ZombieAIMode.DEAD;
			if(!enabledRagdoll) {
				setRagdoll(true);
				enabledRagdoll = true;
			}
		}

		switch(currentMode) {
		case ZombieAIMode.IDLE:
			idleLogic();
			break;
		case ZombieAIMode.CHASE_PLAYER:
			chaseLogic();
			break;
		}
	}

	void updateSound() {
		if(characterSound == null || characterSound.isPlaying)
			return;

		AudioClip clipToPlay = null;
		switch(currentMode) {
		case ZombieAIMode.IDLE:
				clipToPlay = RandomHelper.GetRandomAudioClip(ref IdleSounds);
			break;
		case ZombieAIMode.CHASE_PLAYER:
				clipToPlay = RandomHelper.GetRandomAudioClip(ref ChaseSounds);
			break;
		case ZombieAIMode.TONGUE_PLAYER:
				clipToPlay = RandomHelper.GetRandomAudioClip(ref TongueSounds);
			break;
		}

		if(clipToPlay != null && !characterSound.isPlaying)
		{
			//characterSound.PlayOneShot(clipToPlay);
			characterSound.clip = clipToPlay;
			characterSound.Play();
		}
		
	}

	void updatePhysics() {
		if(charCtrl != null && charCtrl.enabled) {
			charCtrl.Move(Physics.gravity*Time.deltaTime);
		}
	}
		
	// Update is called once per frame
	void Update () {
		updateAILogic();
		updateSound();
		updatePhysics();
	}
}
