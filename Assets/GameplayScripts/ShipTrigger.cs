﻿using UnityEngine;
using System.Collections;

public class ShipTrigger : MonoBehaviour {

	bool isPlayingCutscene = false;
	public Camera cutsceneCamera;
	public Transform endPoint;
	public Transform thrusterTransform;
	public Transform startPoint;
	public GameObject thrusterPrefab;

	PlayerEntity playerEnt;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if(isPlayingCutscene) {
			PlayerGUI gui = FindObjectOfType<PlayerGUI>();
			if(gui.hasFinishedEndingFading()) {
				SetupCutscene(playerEnt);
				this.transform.position = Vector3.Lerp(startPoint.transform.position, endPoint.transform.position, gui.getWinTimeFadePercent()); 
				Vector3 toEndPoint = endPoint.transform.position - startPoint.transform.position;
				toEndPoint.Normalize();
				this.transform.right = toEndPoint;
			}
		}
	}

	void SetupCutscene(PlayerEntity ent) {
		ent.Health = float.PositiveInfinity;
		ent.transform.position = Vector3.up*9999.0f;
		Renderer[] shapes = ent.gameObject.GetComponentsInChildren<Renderer>();
		foreach(Renderer shape in shapes) {
			shape.enabled = false;
		}
		cutsceneCamera.enabled = true;
		cutsceneCamera.transform.LookAt(this.transform);
		Camera[] camera = ent.gameObject.GetComponentsInChildren<Camera>();
		foreach(Camera cam in camera) {
			cam.enabled = false;
		}
	}

	void OnTriggerEnter(Collider other) {

		PlayerEntity ent = other.GetComponent<PlayerEntity>();
		if(ent != null && ent.team == PlayerEntity.Team.PLAYER) {
			PlayerGUI gui = FindObjectOfType<PlayerGUI>();
			playerEnt = ent;
			if(gui != null && gui.HasAllMemories()) {
				isPlayingCutscene = true;
				gui.startEndingFading();
				//gui.SetObjective("You win!");
				GameObject thruster = (GameObject)Instantiate(thrusterPrefab);
				thruster.transform.position = thrusterTransform.position;
				thruster.transform.parent = thrusterTransform;
			}
		}
	}
}
