﻿using UnityEngine;
using System.Collections;

public class PlayerGUI : MonoBehaviour {
	public int levelIndex = 0;
	public PlayerEntity ActivePlayer;
	public GUITexture[] Hearts;
	public Color HeartActivatedColor;
	public Color HeartDeactivatedColor;

	public GUITexture ItemIcon;
	bool hasPlayedItemAnimation = false;
	float elapsedItemVisibleTime = 3.0f;
	float ITEM_ICON_FADE_IN_TIME = 1.0f;
	float ITEM_ICON_VISIBLE_TIME = 2.0f;

	public GameObject TomFace;
	public GUITexture MemoryGUI;
	public Texture2D[] MemoryTextures;

	public GUIText ObjectiveGUI;
	public GUIText DeathState;
	public GUIText DeathTimer;
	public GUITexture DeathBG;

	float elapsedObjectiveFlashTime = 0;
	float MAX_OBJECTIVE_FLASH_TIME = 6;
	float NUM_OBJECTIVE_FLASHES = 4;

	bool isDead = false;
	float elapsedDeathScreenTime = 0.0f;
	float MAX_DEATH_SCREEN_TIME = 1.0f;

	public GUITexture WinBG;
	public GUIText WinState;
	bool hasWon = false;
	bool canReplay = false;
	float elapsedFadeWinTime0 = 0.0f;
	float MAX_WIN_FADE_TIME0 = 5.0f;
	float elapsedFadeWinTime1 = 0.0f;
	float MAX_WIN_FADE_TIME1 = 10.0f;
	float WIN_FADE_TIME1_DELAY = 5.0f;
	float TIME_BEFORE_REPLAY = 15.0f;

	int numItemsCollected = 0;

	public Vector2 animationStartEndPositions;
	float elapsedAnimationTime;
	float elapsedVisibleTime;
	float MAX_VISIBLE_TIME = 2.0f;
	float MAX_TRANSITION_TIME = 1.0f;
	bool hasPlayedAnimation = false;
	bool hasTransitionedUp = false;

	// Use this for initialization
	void Start () {
		SetObjective("Collect Items");
		SetDeathState(false);
	}

	void updateHearts() {
		int numActiveHearts = Mathf.Clamp((int)(Hearts.Length*ActivePlayer.GetHealthPercentage()), 0, Hearts.Length);
		for(int i = 0; i < numActiveHearts; i++) {
			Hearts[i].color = HeartActivatedColor;
		}
		for(int i = numActiveHearts; i < Hearts.Length; i++) {
			Hearts[i].color = HeartDeactivatedColor;
		}
	}

	public void IncrementMemory(Texture2D itemTexture) {
		ItemIcon.texture = itemTexture;
		numItemsCollected = Mathf.Clamp(numItemsCollected+1, 0, MemoryTextures.Length-1);
		MemoryGUI.texture = MemoryTextures[numItemsCollected];
		hasTransitionedUp = false;
		hasPlayedAnimation = false;
		hasPlayedItemAnimation = false;
		elapsedItemVisibleTime = 0.0f;
		elapsedAnimationTime = 0.0f;
		elapsedVisibleTime = 0.0f;
	}

	public bool HasAllMemories() {
		return numItemsCollected == MemoryTextures.Length-1;
	}

	void updateItemAnimation() {
		elapsedItemVisibleTime += Time.deltaTime;
		Color color = ItemIcon.color;
		color.a = Mathf.Clamp01(elapsedItemVisibleTime / ITEM_ICON_FADE_IN_TIME);
		if(elapsedItemVisibleTime > ITEM_ICON_VISIBLE_TIME) {
			hasPlayedItemAnimation = true;
			color.a = 0.0f;
		}
		ItemIcon.color = color;
	}

	void updateMemoryIcon() {
		if(hasPlayedAnimation || !hasPlayedItemAnimation)
			return;

		float value = 0.0f;
		if(!hasTransitionedUp) {
			Color color = MemoryGUI.color;
			color.a = 0.0f;
			MemoryGUI.color = color;
			elapsedAnimationTime += Time.deltaTime;
			value = Mathf.Clamp01(elapsedAnimationTime / MAX_TRANSITION_TIME);
			if(elapsedAnimationTime >= MAX_TRANSITION_TIME) {
				hasTransitionedUp = true;
				elapsedAnimationTime = 0.0f;
			}
		}
		if(hasTransitionedUp) {
			elapsedVisibleTime += Time.deltaTime;
			value = 1.0f;
			Color color = MemoryGUI.color;
			color.a = Mathf.Lerp(0.0f, 1.0f, Mathf.Clamp01(elapsedVisibleTime / MAX_VISIBLE_TIME));
			MemoryGUI.color = color;
			if(elapsedVisibleTime >= MAX_VISIBLE_TIME) {
				elapsedAnimationTime += Time.deltaTime;
				value = 1.0f - Mathf.Clamp01(elapsedAnimationTime / MAX_TRANSITION_TIME);
				if(elapsedAnimationTime >= MAX_TRANSITION_TIME) {
					hasPlayedAnimation = true;
				}
			}
		}

		Vector3 pos = TomFace.transform.localPosition;
		pos.y = Mathf.Lerp(animationStartEndPositions.x, animationStartEndPositions.y, value);
		TomFace.transform.localPosition = pos; 
	}

	public void SetObjective(string message) {
		ObjectiveGUI.text = message;
		elapsedObjectiveFlashTime = 0.0f;
	}

	public void SetDeathState(bool isDead) {
		this.isDead = isDead;
		elapsedDeathScreenTime = 0.0f;

		Color deathColor = DeathState.color;
		Color deathTimerColor = DeathTimer.color;
		if(isDead) {
			deathColor.a = 1.0f;
			deathTimerColor.a = 1.0f;
		}
		else {
			deathColor.a = 0.0f;
			deathTimerColor.a = 0.0f;
		}
		DeathState.color = deathColor;
		DeathTimer.color = deathTimerColor;
	}

	public void SetDeathTimeTilRespawn(float value) {
		int timeTilRespawn = (int)value;
		DeathTimer.text = "Respawn in " + timeTilRespawn;
	}

	void updateDeathScreen() {
		Rect bgRect = DeathBG.pixelInset;
		bgRect.width = Screen.width;
		bgRect.height = Screen.height;
		DeathBG.pixelInset = bgRect;
		elapsedDeathScreenTime += Time.deltaTime;
		float val = Mathf.Clamp01(elapsedDeathScreenTime/MAX_DEATH_SCREEN_TIME);
		Color bgColor = DeathBG.color;
		if(isDead) {
			bgColor.a = val;
		} else {
			bgColor.a = 1.0f-val;
		}
		DeathBG.color = bgColor;
	}

	void updateObjectiveAnimation() {
		if(elapsedObjectiveFlashTime <= MAX_OBJECTIVE_FLASH_TIME) {
			elapsedObjectiveFlashTime += Time.deltaTime;
			Color color = ObjectiveGUI.color;
			float t = Mathf.Clamp01(elapsedObjectiveFlashTime / MAX_OBJECTIVE_FLASH_TIME)*(4.0f*NUM_OBJECTIVE_FLASHES - 2.0f);
			//Carefully chosen sin wave
			color.a = Mathf.Sin((t-1)*Mathf.PI*0.5f)*0.5f+0.5f;
			ObjectiveGUI.color = color;
		}
	}

	public void startEndingFading() {
		elapsedFadeWinTime0 = 0.0f;
		elapsedFadeWinTime1 = 0.0f;
		hasWon = true;
	}

	public bool hasFinishedEndingFading() {
		return elapsedFadeWinTime0 >= 0.5f*MAX_WIN_FADE_TIME0;
	}

	public float getWinTimeFadePercent() {
		return Mathf.Clamp01(elapsedFadeWinTime1/MAX_WIN_FADE_TIME1);
	}


	void updateWinFade() {
		Rect bgRect = WinBG.pixelInset;
		bgRect.width = Screen.width;
		bgRect.height = Screen.height;
		WinBG.pixelInset = bgRect;
		if(hasWon == true) {
			Color bgColor = WinBG.color;

			float val = 0.0f;
			if(elapsedFadeWinTime0 <= MAX_WIN_FADE_TIME0) {
				elapsedFadeWinTime0 += Time.deltaTime;
				val = Mathf.Clamp01(elapsedFadeWinTime0/MAX_WIN_FADE_TIME0);
				bgColor.a = 1.0f-(Mathf.Cos(val*2.0f*Mathf.PI)*0.5f+0.5f);
				Debug.Log("ALPHA IS " + bgColor.a);
			} else {
				elapsedFadeWinTime1 += Time.deltaTime;
				val = Mathf.Clamp01((elapsedFadeWinTime1-WIN_FADE_TIME1_DELAY)/MAX_WIN_FADE_TIME1);
				Color winStateColor = WinState.color;
				winStateColor.a = val;
				bgColor.a = val;
				if(val >= 1.0f && elapsedFadeWinTime1 >= TIME_BEFORE_REPLAY) {
					canReplay = true;
				}
				WinState.color = winStateColor;
			}
			WinBG.color = bgColor;
		}
	}


	// Update is called once per frame
	void Update () {
		updateHearts();
		updateMemoryIcon();
		updateObjectiveAnimation();
		updateDeathScreen();
		updateWinFade();
		updateItemAnimation();

		if(Input.GetKeyUp(KeyCode.P)) {
			Application.LoadLevel(levelIndex);
		}
	}
}
