﻿using UnityEngine;
using System.Collections;

public static class PlacementHelper {
	public static bool DidHitGeometry(Vector3 randPos, out Vector3 geomPos) {
		Vector3 normal;
		return DidHitGeometry(randPos, out geomPos, out normal);
	}
	public static bool DidHitGeometry(Vector3 randPos, out Vector3 geomPos, out Vector3 normal) {
		bool validHit = false;
		geomPos = Vector3.zero;
		normal = Vector3.up;
		Ray rayU = new Ray(randPos, Vector3.up);
		Ray rayD = new Ray(randPos, Vector3.down);
		RaycastHit infoU;
		RaycastHit infoD;
		bool hitU = Physics.Raycast(rayU, out infoU);
		bool hitD = Physics.Raycast(rayD, out infoD);
		Vector3 spawnPosU = rayU.origin + rayU.direction*infoU.distance;
		Vector3 spawnPosD = rayD.origin + rayD.direction*infoD.distance;
		if(hitU && hitD) {
			geomPos = (infoU.distance < infoD.distance) ? spawnPosU : spawnPosD;
			normal = (infoU.distance < infoD.distance) ? infoU.normal : infoD.normal;
			validHit = true;
		} else if(hitU) {
			geomPos = spawnPosU;
			normal = infoU.normal;
			validHit = true;
		} else if(hitD) {
			geomPos = spawnPosD;
			normal = infoD.normal;
			validHit = true;
		}
		return validHit;
	}
}
