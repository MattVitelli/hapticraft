﻿using UnityEngine;
using System.Collections;

public class ItemScript : MonoBehaviour {

	public AudioClip PickupSound;
	public float PickupVolume = 1.0f;
	public GameObject ParticlePrefab;
	bool hasBeenCollected = false;
	float ITEM_ROTATE_SPEED = 90;
	public Texture2D Icon;

	// Use this for initialization
	void Start () {
		GameObject particles = (GameObject)Instantiate(ParticlePrefab);
		particles.transform.position = this.transform.position;
		particles.transform.parent = this.transform;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 rot = this.transform.localEulerAngles;
		rot.y += Time.deltaTime*ITEM_ROTATE_SPEED;
		this.transform.localEulerAngles = rot;
	}

	void OnTriggerEnter(Collider other) {
		PlayerEntity ent = other.gameObject.GetComponent<PlayerEntity>();
		Debug.Log("Collided!");
		if(ent != null && ent.team == PlayerEntity.Team.PLAYER && !hasBeenCollected) {
			AudioSource.PlayClipAtPoint(PickupSound, other.transform.position, PickupVolume);
			PlayerGUI gui = FindObjectOfType<PlayerGUI>();
			gui.IncrementMemory(Icon);
			if(gui.HasAllMemories()) {
				gui.SetObjective("Return to ship");
			}
			hasBeenCollected = true;
			Destroy(this.gameObject);
		}
	}
}
