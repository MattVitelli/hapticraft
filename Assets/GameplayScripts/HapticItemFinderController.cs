﻿using UnityEngine;
using System.Collections;

public class HapticItemFinderController : MonoBehaviour {
	public Transform ItemFinder;
	public Transform ItemArrow;
	public Camera PlayerCamera;
	public Haptics haptics;
	public float MAX_DIST_TO_ITEM_FORCE = 400.0f;

	void SetItemFinderEnabled(bool enabled) {
		MeshRenderer[] meshes = ItemFinder.gameObject.GetComponentsInChildren<MeshRenderer>();
		foreach(MeshRenderer mr in meshes) {
			mr.enabled = enabled;
		}
		
		SkinnedMeshRenderer[] meshes_skinned = ItemFinder.gameObject.GetComponentsInChildren<SkinnedMeshRenderer>();
		foreach(SkinnedMeshRenderer mr in meshes_skinned) {
			mr.enabled = enabled;
		}
	}

	Transform GetNearestItem(Vector3 worldPos) {
		ItemScript[] items = FindObjectsOfType<ItemScript>();
		float minDist = float.PositiveInfinity;
		Transform bestTransform = null;
		foreach(ItemScript item in items) {
			float dist = (item.transform.position - worldPos).magnitude;
			if(dist < minDist) {
				bestTransform = item.transform;
				minDist = dist;
			}
		}
		if(items.Length == 0) {
			ShipTrigger ship = FindObjectOfType<ShipTrigger>();
			bestTransform = ship.transform;
		}
		return bestTransform;
	}

	// Use this for initialization
	void Start () {
		SetItemFinderEnabled(true);
	}

	void OnEnable() {
		SetItemFinderEnabled(true);
	}

	void OnDisable () {
		SetItemFinderEnabled(false);
	}

	// Update is called once per frame
	void Update () {
		Vector3 hapticDevicePosition = haptics.GetHapticPosition() / Haptics.HAPTIC_WORKSPACE;
		Vector3 itemArrowPos = ItemArrow.transform.position;
		Transform nearestItem = GetNearestItem(itemArrowPos);
		Vector3 rot = ItemArrow.transform.localEulerAngles;
		if(nearestItem != null) {
			Vector3 toItem = ItemFinder.transform.InverseTransformPoint(nearestItem.transform.position);
			float angleToItem = Mathf.Rad2Deg*Mathf.Atan2(toItem.x, toItem.z);
			rot.y = angleToItem;
			float distToItem = (new Vector3(toItem.x, 0, toItem.z)).magnitude;
			toItem.Normalize();
			float force = Mathf.Clamp01(1.0f-distToItem/MAX_DIST_TO_ITEM_FORCE)*800.0f;
			haptics.SetForceCenter(force, toItem * 0.03f);
		} else {
			haptics.SetForceCenter(0, 0, 0, 0);
			rot.y = 0;
		}
		haptics.SetDamping(10);
		haptics.SetDrillRate(0, Vector3.zero);
		ItemArrow.transform.localEulerAngles = rot;
	}
}
