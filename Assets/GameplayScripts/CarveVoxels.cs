﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CarveVoxels : MonoBehaviour {
	List<TerrainGenerator> generatorsToUpdate = new List<TerrainGenerator>();
	public float brushRadius = 1.0f;
	public bool isPainting = false;
	public int paintMaterialIndex = 0;
	public bool isEroding = true;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButton(0)) {
			Camera camera = this.camera;
			Ray ray = camera.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
			//Ray ray = new Ray(camera.transform.position, camera.transform.forward);
			RaycastHit info;
			if(Physics.Raycast(ray, out info)) {
				Debug.DrawRay(ray.origin, ray.direction * info.distance, Color.red);
				if(GetTerrainGeneratorsToUpdate(info.transform)) {
					Debug.Log("Beginning Erosion");
					foreach(TerrainGenerator chunk in generatorsToUpdate) {
						chunk.ModifyDensityField(info.point, brushRadius, isEroding, isPainting, paintMaterialIndex);
					}
				}
			}
		}
	}

	bool GetTerrainGeneratorsToUpdate(Transform srcTransform) {
		generatorsToUpdate.Clear();
		Transform t = srcTransform;
		TerrainGenerator gen = t.gameObject.GetComponent<TerrainGenerator>();
		while(gen == null && t.parent != null) {
			//Update parent
			t = t.parent;
			//Update generator
			gen = t.gameObject.GetComponent<TerrainGenerator>();
		}
		
		if(gen != null) {
			Debug.Log("Found object!");
			generatorsToUpdate.Add(gen);
			//If there's a parent, check if parent has terrain manager
			//Otherwise, return null
			TerrainManager mgr = (t.parent != null ? t.parent.gameObject.GetComponent<TerrainManager>() : null);
			if(mgr != null) {
				//Update neighboring voxels
				generatorsToUpdate.AddRange(mgr.GetNeighboringVoxels(gen));
			}
		}
		return generatorsToUpdate.Count > 0;
	}
}
