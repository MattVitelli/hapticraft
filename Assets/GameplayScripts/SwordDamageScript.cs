﻿using UnityEngine;
using System.Collections;

public class SwordDamageScript : MonoBehaviour {
	public float MaxDamage = 50;
	public GameObject bloodPrefab;
	public float MagnitudeDamageThreshold = 5;
	public float MaxVelocity = 2.3f;
	public Transform tipPosition;
	public Vector3 velocity;
	public Vector3 collisionPos;
	public bool isCollision = false;
	public Vector3 collisionNormal;
	public float penetrationDist = 0;
	public Transform collisionBody;
	public Transform HapticSwordHandle;
	public Vector3 HapticSwordPos;
	public bool hasNotPierced = true;

	int numContactedPoints = 0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//oldTipPos = tipPosition.position;
	}

	bool isTerrain(Collision collision) {
		return (collision.gameObject.tag == "TERRAIN");
	}

	void OnCollisionEnter(Collision collision) {
		if(isTerrain(collision)) {
			Debug.Log("This shouldn't happen!");
			return;
		}
		numContactedPoints++;
		float mag = velocity.magnitude;
		ContactPoint contact = collision.contacts[0];

		Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
		Vector3 pos = contact.point;

		isCollision = true;
		this.renderer.material.color = Color.red;
		if(numContactedPoints == 1) {
			collisionPos = pos;
		}
		collisionNormal = contact.normal;
		penetrationDist = (tipPosition.position-collisionPos).magnitude;
		collisionBody = contact.otherCollider.transform;
		HapticSwordPos = HapticSwordHandle.localPosition;
		hasNotPierced = mag < MagnitudeDamageThreshold;
		//Debug.Log("Magnitude is " + mag);
		PlayerEntity collidedEntity = GetPlayerEntity(collision.transform);
		if(!hasNotPierced && collidedEntity != null && numContactedPoints <= 1) {
			GameObject g = (GameObject)Instantiate(bloodPrefab, pos, rot);
			g.transform.parent = collision.transform;
			//collidedEntity.ApplyDamage(Mathf.Clamp01(mag / MaxVelocity)*MaxDamage);
		}
	}

	void OnCollisionStay(Collision collisionInfo) {
		if(isTerrain(collisionInfo))
			return;
		foreach (ContactPoint contact in collisionInfo.contacts) {
			if(contact.otherCollider.transform == collisionBody) {
				PlayerEntity collidedEntity = GetPlayerEntity(collisionBody);
				if(collidedEntity != null) {
					float damage = velocity.magnitude*MaxDamage;
					collidedEntity.ApplyDamage(damage);
				}
			}
		}
	}
	
	void OnCollisionExit(Collision collision) {
		if(isTerrain(collision))
			return;
		numContactedPoints--;
		if (numContactedPoints < 0) {
			numContactedPoints = 0;
		}
		if(numContactedPoints == 0)	{
			ContactPoint contact = collision.contacts[0];
			Vector3 pos = contact.point;
			Quaternion rot = Quaternion.FromToRotation(Vector3.up, contact.normal);
			PlayerEntity collidedEntity = GetPlayerEntity(collision.transform);
			if(collidedEntity != null) {
				GameObject g = (GameObject)Instantiate(bloodPrefab, pos, rot);
				g.transform.parent = collision.transform;
				//float damage = (collisionPos-pos).magnitude*MaxDamage;
				//collidedEntity.ApplyDamage(damage);
			}
			isCollision = false;
			this.renderer.material.color = Color.gray;
		}
	}

	//ATP add
	void OnDisable() {
		isCollision = false;
		this.renderer.material.color = Color.gray;
		hasNotPierced = true;
		numContactedPoints = 0;
	}


	PlayerEntity GetPlayerEntity(Transform t) {
		Transform temp = t;
		PlayerEntity ent = temp.gameObject.GetComponent<PlayerEntity>();
		while(ent == null && temp.parent != null) {
			temp = temp.parent;
			ent = temp.gameObject.GetComponent<PlayerEntity>();
		}
		return ent;
	}
}
