﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class BSplineHelper {

	public static SortedList<string, int> binomailMemoizer = new SortedList<string, int>();
	
	//Recursively construct binomial coefficients w/ memoizer for increased performance
	static int GenerateBinomialCoeffs(int n, int k, SortedList<string, int> memoizer)
	{
		if (k == 0) return 1;
		if (n == 0) return 0;
		string key = n + " " + k;
		if (memoizer.ContainsKey(key)) return memoizer[key];
		
		int coeff = GenerateBinomialCoeffs(n - 1, k - 1, memoizer) + GenerateBinomialCoeffs(n - 1, k, memoizer);
		if (!memoizer.ContainsKey(key)) memoizer.Add(key, coeff);
		
		return coeff;
	}
	
	public static int GetBinomailCoeff(int n, int k)
	{
		return GenerateBinomialCoeffs(n, k, binomailMemoizer);
	}

	public static Vector3 EvaluateAtPoint(ref Vector3[] points, float value)
	{
		float invT = 1.0f - value;
		int n = points.Length - 1;
		Vector3 sum = Vector3.zero;
		for (int i = 0; i < points.Length; i++)
			sum += GenerateBinomialCoeffs(n, i, binomailMemoizer) * (float)Mathf.Pow(invT, n - i) * (float)Mathf.Pow(value, i) * points[i];

		return sum;
	}
}

/*
public class BSpline {
	Vector3 ptOrigin;
	Vector3 ptEnd;
	Vector3 pt0;
	Vector3 pt1;

	public float evaluateSpline(float t) {

	}

	public Vector3 splineDerivative(float t) {

	}
}
*/