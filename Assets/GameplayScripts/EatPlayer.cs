﻿using UnityEngine;
using System.Collections;

public class EatPlayer : MonoBehaviour {
	public Transform playerToEat;
	public float speed=50;
	// Use this for initialization
	void Start () {
		PlayerEntity[] ents = Object.FindObjectsOfType<PlayerEntity>();
		foreach(PlayerEntity ent in ents) {
			if(ent.team == PlayerEntity.Team.PLAYER) {
				playerToEat = ent.transform;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		this.rigidbody.WakeUp();
		Vector3 toPlayer = playerToEat.position - this.rigidbody.position;
		float distToPlayer = toPlayer.magnitude;
		float maxForcePerSec = Mathf.Min(speed, speed*distToPlayer)*rigidbody.mass;
		toPlayer.Normalize();
		this.rigidbody.AddForce(toPlayer*maxForcePerSec);
	}
}
