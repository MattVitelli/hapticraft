﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour {
	public GameObject[] Enemies;
	public int MaxEnemyCount = 30;
	public int MaxEnemiesPerWave = 5;
	public int enemyCount = 0;
	public float MinEnemyDistFromPlayer = 300;
	public float MaxEnemyDistFromPlayer = 500;
	public float EnemyUnspawnDistance = 500;
	public float TimeBetweenEnemyWaves = 60*2; //60 sec * num_minutes between waves
	float elapsedTime = float.PositiveInfinity;
	public List<GameObject> spawnedEnemies = new List<GameObject>();
	List<GameObject> enemiesToDelete = new List<GameObject>();
	public Camera playerCamera;

	public float MinEnemyTimeDead = 30.0f;

	System.Random rand = new System.Random();
	public TerrainManager manager;
	// Use this for initialization
	void Start () {
	
	}

	bool IsPosOccluded(Vector3 pos, Vector3 playerPos, Vector3 playerFwd) {
		bool occluded = Vector3.Dot(playerFwd, pos-playerPos)<0.0f;
		if(!occluded) {
			Vector3 toPos = pos-playerPos;
			float distToPos = toPos.magnitude;
			toPos.Normalize();
			Ray ray = new Ray(playerPos, toPos);
			occluded |= Physics.Raycast(ray, distToPos);
		}
		return occluded;
	}

	Vector3 GetPositionBehindPlayer(Vector3 playerPos, Vector3 playerFwd) {
		while(true) {
			float dist = Mathf.Lerp(MinEnemyDistFromPlayer, MaxEnemyDistFromPlayer, Random.value);
			Vector3 randPos = Random.onUnitSphere*dist + playerPos;
			Ray rayU = new Ray(randPos, Vector3.up);
			Ray rayD = new Ray(randPos, Vector3.down);
			RaycastHit hitU;
			RaycastHit hitD;
			bool wasHitU = Physics.Raycast(rayU, out hitU, MaxEnemyDistFromPlayer);
			bool wasHitD = Physics.Raycast(rayD, out hitD, MaxEnemyDistFromPlayer);
			if(wasHitU || wasHitD) {

				Vector3 posU = rayU.origin + rayU.direction * hitU.distance;
				bool posUOccluded = wasHitU && IsPosOccluded(posU, playerPos, playerFwd);
				Vector3 posD = rayD.origin + rayD.direction * hitD.distance;
				bool posDOccluded = wasHitD && IsPosOccluded(posD, playerPos, playerFwd);

				if(posUOccluded && posDOccluded) {
					return (hitU.distance < hitD.distance) ? posU : posD;
				}
				else if(posUOccluded) {
					return posU;
				}
				else if(posDOccluded) {
					return posD;
				}
			}
		}
	}

	void SpawnEnemiesAroundPlayer() {
		int numEnemiesToSpawn = Mathf.Min(MaxEnemiesPerWave, MaxEnemyCount-enemyCount);
		Vector3 playerPos = playerCamera.transform.position;
		Vector3 playerFwd = playerCamera.transform.forward;
		for(int i = 0; i < numEnemiesToSpawn; i++) {
			Vector3 enemyPos = GetPositionBehindPlayer(playerPos, playerFwd);
			int randEnemyIdxToSpawn = rand.Next(Enemies.Length);
			Collider collider = Enemies[randEnemyIdxToSpawn].GetComponent<Collider>();
			enemyPos += Vector3.up*collider.bounds.extents.y*1.01f;
			if(randEnemyIdxToSpawn == 1)
				enemyPos += Vector3.up*20;
			GameObject enemy = (GameObject)Instantiate(Enemies[randEnemyIdxToSpawn], enemyPos, Quaternion.identity);
			enemyCount++;
			spawnedEnemies.Add(enemy);
		}
	}

	void CleanupDeadEnemies() {
		enemiesToDelete.Clear();
		for(int i = 0; i < spawnedEnemies.Count; i++) {
			PlayerEntity ent = spawnedEnemies[i].GetComponent<PlayerEntity>();
			bool isDead = ent.IsDead && ent.TimeDead >= MinEnemyTimeDead;
			Vector3 toTarget =  spawnedEnemies[i].transform.position - playerCamera.transform.position;
			bool farAway = (Vector3.Dot(toTarget, playerCamera.transform.forward) < 0.0f && toTarget.magnitude >= EnemyUnspawnDistance);
			if(isDead || farAway) {
				enemyCount = Mathf.Max(0, enemyCount-1);
				enemiesToDelete.Add(spawnedEnemies[i]);
			}
		}
		for(int i = 0; i < enemiesToDelete.Count; i++) {
			spawnedEnemies.Remove(enemiesToDelete[i]);
			Destroy(enemiesToDelete[i]);
		}
	}
	
	// Update is called once per frame
	void Update () {
		elapsedTime += Time.deltaTime;
		if(elapsedTime >= TimeBetweenEnemyWaves && manager.HasGeneratedSufficientChunks()) {
			elapsedTime = 0.0f;
			CleanupDeadEnemies();
			SpawnEnemiesAroundPlayer();
		}
	}
}
