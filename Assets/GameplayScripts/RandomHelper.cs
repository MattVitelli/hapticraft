﻿using UnityEngine;
using System.Collections;


public static class RandomHelper {
	static System.Random rand = new System.Random();
	public static int GetRandomIntInRange(int maxRange) {
		return rand.Next(maxRange);
	}

	public static AudioClip GetRandomAudioClip(ref AudioClip[] clips) {
		return clips[rand.Next(clips.Length)];
	}
}