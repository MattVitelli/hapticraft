﻿using UnityEngine;
using System.Collections;

public class DeleteParticles : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(this.particleSystem != null && this.particleSystem.isStopped) {
			DestroyImmediate(this.gameObject);
		}
	}
}
