
Shader "Noise/PerlinNoise3D" 
{
	Properties 
	{
	}
	SubShader 
	{
		Blend One One
		
    	Pass 
    	{
    		ZTest Always Cull Off ZWrite Off
      		Fog { Mode off }

			CGPROGRAM
			#pragma target 3.0
			#pragma vertex vert
			#pragma fragment frag
			
			struct a2v 
			{
    			float4  pos : SV_POSITION;
    			float2  uv : TEXCOORD0;
			};

			struct v2f 
			{
    			float4  pos : SV_POSITION;
    			float2  uv : TEXCOORD0;
			};

			v2f vert(a2v v)
			{
    			v2f OUT;
    			OUT.pos = mul(UNITY_MATRIX_MVP, v.pos);
    			OUT.uv = v.uv;
    			return OUT;
			}
			
			uniform sampler2D _NoiseTex;
			uniform float4 _NoiseParams;
			uniform float2 _InvTexSize;
			
			float4 GetSmoothNoise(float2 uv, float freq, sampler2D noiseMap, float2 invRes)
			{
				float2 crd = freq*invRes;
				float2 f = frac( uv * freq*invRes );
				float4 h0 = tex2D(noiseMap, uv);
				float4 h1 = tex2D(noiseMap, uv+float2(crd.x,0));
				float4 h2 = tex2D(noiseMap, uv+float2(0,crd.y));
				float4 h3 = tex2D(noiseMap, uv+crd);
				float4 tA = lerp(h0,h1,f.x);
				float4 tB = lerp(h2,h3,f.x);
			    return lerp(tA, tB, f.y);
			}
			
			#define MAX_ITER 16
						
			float4 frag(v2f IN) : COLOR
			{
				float2 pos = IN.uv;// * _TexSize0;
				float z = 0;
				//float noise = NOISE2D(pos.x*_Gain/_Frq, pos.y*_Gain/_Frq) * _Amp/_Gain;
				float4 FinalValue = 0;
				float freq = _NoiseParams.x;
				float amp = _NoiseParams.y;
				return GetSmoothNoise(pos * freq, freq, _NoiseTex, _InvTexSize) * amp;
			}
			
		ENDCG

    	}
	}
}