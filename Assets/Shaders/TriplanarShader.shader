﻿Shader "Custom/TriplanarShader" {
	Properties {
		_BaseTex0 ("BaseMap 0", 2D) = "white" {}
		_BumpTex0 ("NormalMap 0", 2D) = "bump" {}
		_BaseTex1 ("BaseMap 1", 2D) = "white" {}
		_BumpTex1 ("NormalMap 1", 2D) = "bump" {}
		_BaseTex2 ("BaseMap 2", 2D) = "white" {}
		_BumpTex2 ("NormalMap 2", 2D) = "bump" {}
		_BaseTex3 ("BaseMap 3", 2D) = "white" {}
		_BumpTex3 ("NormalMap 3", 2D) = "bump" {}
		_Frequency("Frequency",Float) = 0.01
		_SignFix ("Sign Fix", Vector) = (-1,-1,1,0)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Lambert vertex:vert

		sampler2D _BaseTex0;
		sampler2D _BaseTex1;
		sampler2D _BaseTex2;
		sampler2D _BaseTex3;
		sampler2D _BumpTex0;
		sampler2D _BumpTex1;
		sampler2D _BumpTex2;
		sampler2D _BumpTex3;
		float _Frequency;
		float4 _SignFix;

		struct Input {
			float3 worldPos;
			float3 customNormal;
			float4 blendColor;
		};
		
		void vert (inout appdata_full v, out Input o) {
          UNITY_INITIALIZE_OUTPUT(Input,o);
          o.customNormal = v.normal;
          o.blendColor = v.color;
      	}

		void surf (Input IN, inout SurfaceOutput o) {
			float3 blendWeightProj = 7.0*(abs(IN.customNormal)-0.2);
			blendWeightProj = pow(blendWeightProj,3);
			blendWeightProj = max(blendWeightProj, 0); 
			blendWeightProj /= dot(blendWeightProj,1); //Normalize blend weights
			float4 blendWeightTex = IN.blendColor / dot(IN.blendColor,1);
			
			float3 TC = IN.worldPos*_Frequency;
			float3 N = normalize(IN.customNormal);
			float3 T = normalize(N.zxy - dot(N.zxy, N)*N); //USE ME FOR EVERYTHING EXCEPT -Z
			float3 B = normalize(cross(N, T));
			
			// Compute projections
			float4 czy = tex2D(_BaseTex0, TC.zy)*blendWeightTex.x +
						 tex2D(_BaseTex1, TC.zy)*blendWeightTex.y +
						 tex2D(_BaseTex2, TC.zy)*blendWeightTex.z +
						 tex2D(_BaseTex3, TC.zy)*blendWeightTex.w;
						 
			float3 nzy = tex2D(_BumpTex0, TC.zy)*blendWeightTex.x +
						 tex2D(_BumpTex1, TC.zy)*blendWeightTex.y +
						 tex2D(_BumpTex2, TC.zy)*blendWeightTex.z +
						 tex2D(_BumpTex3, TC.zy)*blendWeightTex.w;
						 
			float4 cxz = tex2D(_BaseTex0, TC.xz)*blendWeightTex.x +
						 tex2D(_BaseTex1, TC.xz)*blendWeightTex.y +
						 tex2D(_BaseTex2, TC.xz)*blendWeightTex.z +
						 tex2D(_BaseTex3, TC.xz)*blendWeightTex.w;
						 
			float3 nxz = tex2D(_BumpTex0, TC.xz)*blendWeightTex.x +
						 tex2D(_BumpTex1, TC.xz)*blendWeightTex.y +
						 tex2D(_BumpTex2, TC.xz)*blendWeightTex.z +
						 tex2D(_BumpTex3, TC.xz)*blendWeightTex.w;
						 
			float4 cxy = tex2D(_BaseTex0, TC.xy)*blendWeightTex.x +
						 tex2D(_BaseTex1, TC.xy)*blendWeightTex.y +
						 tex2D(_BaseTex2, TC.xy)*blendWeightTex.z +
						 tex2D(_BaseTex3, TC.xy)*blendWeightTex.w;
						 
			float3 nxy = tex2D(_BumpTex0, TC.xy)*blendWeightTex.x +
						 tex2D(_BumpTex1, TC.xy)*blendWeightTex.y +
						 tex2D(_BumpTex2, TC.xy)*blendWeightTex.z +
						 tex2D(_BumpTex3, TC.xy)*blendWeightTex.w;
						 
			float4 blendColor = 0;
			// Finally, blend the results of the 3 planar projections.  
			blendColor = czy * blendWeightProj.x +  
					cxz * blendWeightProj.y +  
					cxy * blendWeightProj.z;
		
			float3 blendBump = nzy * blendWeightProj.x +  
					   nxz * blendWeightProj.y +  
					   nxy * blendWeightProj.z;
					   
			float3x3 TBN = float3x3(T,B,N);
			blendBump = normalize(mul(TBN,blendBump*2-1));//normalize(mul(float4(mul(blendBump,TBN),0),UNITY_MATRIX_MV).xyz);
			//blendBump.x *= -1;
			o.Albedo = blendColor.rgb;
			o.Alpha = blendColor.a;
			o.Normal = fixed3(blendBump.x,blendBump.y,blendBump.z);//mul(float4(normalize(mul(blendBump,TBN)),0), UNITY_MATRIX_V).xyz;
			//o.Normal = blendBump;//UnpackNormal(float3(0,0,1));//N;//mul(float4(normalize(mul(blendBump,TBN)),0), UNITY_MATRIX_V).xyz;
		}
		ENDCG
	} 
	FallBack "Diffuse"
}
