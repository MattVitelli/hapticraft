﻿Shader "Custom/SkyShader" {
   Properties {
      _DayTex ("Day Gradient (RGB)", 2D) = "white" {}
	  _DawnTex ("Dawn Gradient (RGB)", 2D) = "white" {}
	  _NightTex ("Night Cubemap (RGB)", Cube) = "white" {}
	  _CloudTex0 ("Cloud Layer 0 (RGB)", 2D) = "white" {}
	  _CloudTex1 ("Cloud Layer 1 (RGB)", 2D) = "white" {}
	  _CloudTex2 ("Cloud Layer 2 (RGB)", 2D) = "white" {}
	  _CloudSpeed0("Cloud Speed 0",Float) = 0.1
	  _CloudHeight0("Cloud Height 0",Float) = 0.1
	  _CloudSpeed1("Cloud Speed 1",Float) = 0.2
	  _CloudHeight1("Cloud Height 1",Float) = 0.1
	  _CloudSpeed2("Cloud Speed 2",Float) = 0.5
	  _CloudHeight2("Cloud Height 2",Float) = 0.1
	  _CloudCoverage("Cloud Coverage",Float) = 0.5
	  _CloudColor("Cloud Color",Color) = (1,1,1,1)
	  _TimeOfDay("TimeOfDay",Range (-1, 1)) = 0.5
	  _DawnSharpness("DawnSharpness",Float) = 1.1
   }
 
   SubShader {
      Tags { "Queue"="Background"  }
 
      Pass {
         ZWrite Off 
         Cull Off 
 
         CGPROGRAM
         #pragma target 3.0
         #pragma vertex vert
         #pragma fragment frag
 
         // User-specified uniforms
         samplerCUBE _NightCube;
         sampler2D _DawnTex;
         sampler2D _DayTex;
         sampler2D _CloudTex0;
         sampler2D _CloudTex1;
         sampler2D _CloudTex2;
         //float4 _Time;
         float _CloudSpeed0;
         float _CloudHeight0;
         float _CloudSpeed1;
         float _CloudHeight1;
         float _CloudSpeed2;
         float _CloudHeight2;
         float _CloudCoverage;
         float4 _CloudColor;
         float _TimeOfDay;
         float _DawnSharpness;
         
 
         struct vertexInput {
            float4 vertex : POSITION;
            float3 texcoord : TEXCOORD0;
         };
 
         struct vertexOutput {
            float4 vertex : SV_POSITION;
            float3 texcoord : TEXCOORD0;
         };
 
         vertexOutput vert(vertexInput input)
         {
            vertexOutput output;
            output.vertex = mul(UNITY_MATRIX_MVP, input.vertex);
            output.texcoord = input.texcoord;
            return output;
         }
 
         fixed4 frag (vertexOutput input) : COLOR
         {
         	const float pi = 3.14159265359;
         	float3 V = normalize(input.texcoord);
         	float4 Night = texCUBE(_NightCube, V);
         	float theta = saturate((atan2(abs(V.z), V.x)/pi));//*0.5+0.5);
         	float2 TC = float2(theta, max(0.001,V.y));
         	float4 Day = tex2D(_DayTex, TC);
         	float4 Dawn = tex2D(_DawnTex, TC);
         	float blendPow = pow(abs(_TimeOfDay),_DawnSharpness);
         	float4 finalColor = lerp(Dawn, Day, blendPow);
         	float4 cloudTerm = 0;
         	float2 tc0 = (_CloudHeight0/V.y)*V.xz + _Time.y*_CloudSpeed0;
         	float2 tc1 = (_CloudHeight1/V.y)*V.xz + _Time.y*_CloudSpeed1;
         	float2 tc2 = (_CloudHeight2/V.y)*V.xz + _Time.y*_CloudSpeed2;
         	cloudTerm += (tex2D(_CloudTex0, tc0) + tex2D(_CloudTex1, tc1) + tex2D(_CloudTex2, tc2))/3.0;
         	cloudTerm = pow(cloudTerm, _CloudCoverage);
         	if(_TimeOfDay < 0.0) {
         		finalColor = lerp(Dawn, Night, blendPow);
         	}
         	finalColor = lerp(finalColor, _CloudColor, cloudTerm);
            return finalColor;
         }
         ENDCG 
      }
   } 	
}