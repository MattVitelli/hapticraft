﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
public class TerrainGenerator : MonoBehaviour {
	public VoxelBlock block;
	public int VoxelGridSize = 4;
	public GameObject blockPrefab;
	public GameObject emptyPrefab;
	public TerrainMaterialHandler materialHandler;
	public float generationScale = 0.2f;
	public TerrainManager manager;
	MarchingCubeBlock[] voxels;
	//public List<MarchingCubeBlock> voxels = new List<MarchingCubeBlock>();

	public bool generatedGeometry = false;
	public int idX;
	public int idY;
	public int idZ;

	const int MINIMUM_EDITABLE_Y = 10;
	// Use this for initialization
	void Start () {
		generateGeometry();
	}

	void generateGeometry() {
		if(!generatedGeometry) {
			generateDensityField();
			int voxelCountX = (this.block.DensityFieldWidth - 1) / VoxelGridSize;
			int voxelCountY = (this.block.DensityFieldHeight - 1) / VoxelGridSize;
			int voxelCountZ = (this.block.DensityFieldDepth - 1) / VoxelGridSize;
			int numVoxels = voxelCountX*voxelCountY*voxelCountZ;
			voxels = new MarchingCubeBlock[numVoxels];
			for(int i = 0; i < voxelCountX; i++) {
				for(int j = 0; j < voxelCountY; j++) {
					for(int k = 0; k < voxelCountZ; k++) {
						GameObject obj = (GameObject)Instantiate(blockPrefab);
						obj.transform.parent = this.transform;
						obj.transform.localScale = Vector3.one;
						obj.transform.localPosition = Vector3.zero;
						obj.name = i + " " + j + " " + k;
						MarchingCubeBlock block = obj.AddComponent<MarchingCubeBlock>();
						block.parent = this;
						block.offsetX = i*VoxelGridSize;
						block.offsetY = j*VoxelGridSize;
						block.offsetZ = k*VoxelGridSize;
						block.width = VoxelGridSize;
						block.height = VoxelGridSize;
						block.depth = VoxelGridSize;
						block.CreateGeometry();
						int stride = i + j*voxelCountX + k*voxelCountX*voxelCountY;
						voxels[stride] = block;
						//voxels.Add(block);
					}
				}
			}
			generatedGeometry = true;
		}
	}

	int computeStrideInVoxelGrid(int i, int j, int k) {
		return i + j * block.DensityFieldWidth + k *block.DensityFieldWidth*block.DensityFieldHeight;
	}

	void generateDensityField() {
		this.block = new VoxelBlock(65, 65, 65);//17,17,17);//33,33,33);//65, 65, 65);

		Vector3 deltaPt = 2.0f*new Vector3(1.0f/(float)(block.DensityFieldWidth-1), 1.0f/(float)(block.DensityFieldHeight-1), 1.0f/(float)(block.DensityFieldDepth-1));
		deltaPt.Scale(this.transform.localScale);
		System.Random rand = new System.Random();
		rand.NextBytes(this.block.MaterialField);
		Vector3 scale = 2.0f*new Vector3(1.0f/(float)(block.DensityFieldWidth-1), 1.0f/(float)(block.DensityFieldHeight-1), 1.0f/(float)(block.DensityFieldDepth-1));
		scale.Scale(this.transform.localScale);
		Vector3 offset = this.transform.position-this.transform.localScale;
		float[] densityField = new float[block.DensityFieldWidth*block.DensityFieldHeight*block.DensityFieldDepth];

		for(int k = 0; k < block.DensityFieldDepth; k++) {
			for(int j = 0; j < block.DensityFieldHeight; j++) {
				for(int i = 0; i < block.DensityFieldWidth; i++) {
					int stride = computeStrideInVoxelGrid(i, j, k);
					Vector3 pt = new Vector3(i,j,k);
					pt.Scale(scale);
					pt += offset;
					densityField[stride] = ProceduralTerrainHelper.GetDensityAtPoint(pt);
					//block.DensityField[stride] = (byte)(255.0f*ProceduralTerrainHelper.GetDensityAtPoint(pt, deltaPt));
					block.MaterialField[stride] = (byte)((block.MaterialField[stride] % (materialHandler.BaseMaps.Length-1)) + 1);
				}
			}
		}

		//Smoothing
		ProceduralTerrainHelper.SmoothDensityField(ref densityField, this.block, deltaPt, scale, offset);
	}

	public void ModifyDensityField(Vector3 ptWorldSpace, float brushRadius, bool subtract, bool paintMode, int paintID) {

		Vector3 ptWorldSpaceMin = ptWorldSpace-Vector3.one*brushRadius;
		Vector3 ptWorldSpaceMax = ptWorldSpace+Vector3.one*brushRadius;
		Vector3 ptLocalSpaceMax = this.transform.InverseTransformPoint(ptWorldSpaceMax);
		Vector3 ptLocalSpaceMin = this.transform.InverseTransformPoint(ptWorldSpaceMin);
		Vector3 delta = new Vector3(1.0f/(float)block.DensityFieldWidth,
		                            1.0f/(float)block.DensityFieldHeight,
		                            1.0f/(float)block.DensityFieldDepth);
		Vector3 boundCheck = Vector3.one + delta*2.01f;
		bool outOfBounds = (ptLocalSpaceMax.x < -boundCheck.x || ptLocalSpaceMin.x > boundCheck.x ||
		                    ptLocalSpaceMax.y < -boundCheck.y || ptLocalSpaceMin.y > boundCheck.y ||
		                    ptLocalSpaceMax.z < -boundCheck.z || ptLocalSpaceMin.z > boundCheck.z);
		if(!outOfBounds) {
			Debug.Log("Eroding density field " + this.gameObject.name);

			Vector3 ptLocalSpace = (this.transform.InverseTransformPoint(ptWorldSpace)+Vector3.one)*0.5f;
			ptLocalSpaceMin = (ptLocalSpaceMin+Vector3.one)*0.5f;
			ptLocalSpaceMax = (ptLocalSpaceMax+Vector3.one)*0.5f;

			//float radiusLocalSpace = (ptLocalSpaceMax-ptLocalSpaceMin).sqrMagnitude;

			int maxDW = block.DensityFieldWidth-1;
			int maxDH = block.DensityFieldHeight-1;
			int maxDD = block.DensityFieldDepth-1;

			/*
			Vector3 res = new Vector3((float)block.DensityFieldWidth-1,
			                          (float)block.DensityFieldHeight-1,
			                          (float)block.DensityFieldDepth-1);
			                          */
			Vector3 res = new Vector3(maxDW, maxDH, maxDD);
			Vector3 invRes = new Vector3(1.0f/res.x, 1.0f/res.y, 1.0f/res.z);
				
			Vector3 rawScaledPtMin = ptLocalSpaceMin;
			//rawScaledPtMin = ptLocalSpace - Vector3.one*radiusLocalSpace;
			rawScaledPtMin.Scale(res);
			Vector3 rawScaledPtMax = ptLocalSpaceMax;
			//rawScaledPtMax = ptLocalSpace + Vector3.one*radiusLocalSpace;
			rawScaledPtMax.Scale(res);

			int minX = Mathf.Clamp(Mathf.FloorToInt(rawScaledPtMin.x),0,maxDW);
			int maxX = Mathf.Clamp(Mathf.CeilToInt(rawScaledPtMax.x),0,maxDW);
			int minY = Mathf.Clamp(Mathf.FloorToInt(rawScaledPtMin.y),0,maxDH);
			int maxY = Mathf.Clamp(Mathf.CeilToInt(rawScaledPtMax.y),0,maxDH);
			int minZ = Mathf.Clamp(Mathf.FloorToInt(rawScaledPtMin.z),0,maxDD);
			int maxZ = Mathf.Clamp(Mathf.CeilToInt(rawScaledPtMax.z),0,maxDD);
			if(minY < MINIMUM_EDITABLE_Y && idY < 0) {
				minY = MINIMUM_EDITABLE_Y;
			}

			float maxRad = (ptLocalSpaceMax-ptLocalSpaceMin).sqrMagnitude;
				//this.transform.InverseTransformPoint(this.transform.position + Vector3.one*brushRadius).sqrMagnitude;

			//Update density field
			for(int k = minZ; k <= maxZ; k++) {
				for(int j = minY; j <= maxY; j++) {
					for(int i = minX; i <= maxX; i++) {
						int stride = computeStrideInVoxelGrid(i, j, k);
						Vector3 p = new Vector3(i, j, k);
						p.Scale(invRes);
						float attn = Mathf.Clamp01(1.0f - (p - ptLocalSpace).sqrMagnitude / maxRad);
						float density = Mathf.Clamp((float)block.DensityField[stride] + (3.0f*attn*(subtract?-1:1)),0,255);
						if(paintMode) {
							block.MaterialField[stride] = (byte)paintID;
						}
						else {
							block.DensityField[stride] = (byte)density;
						}
					}
				}
			}
			Debug.Log("Material ID is " + (byte)paintID);

			Bounds brushBounds = new Bounds();
			brushBounds.SetMinMax(ptWorldSpaceMin, ptWorldSpaceMax);
			Debug.Log("Brush bounds: " + ptWorldSpaceMin.ToString() + " " + ptWorldSpaceMax.ToString());
			for(int i = 0; i < voxels.Length; i++) {
				bool containsOrIntersects = (voxels[i].offsetX <= maxX+1
				                             && voxels[i].offsetX+voxels[i].width >= minX-1
				                             && voxels[i].offsetY <= maxY
				                             && voxels[i].offsetY+voxels[i].height >= minY-1
				                             && voxels[i].offsetZ <= maxZ+1
				                             && voxels[i].offsetZ+voxels[i].depth >= minZ-1);
				if(containsOrIntersects) {
					voxels[i].CreateGeometry(manager, idX, idY, idZ);
				}
			}
			/*
			int voxelCountX = (this.block.DensityFieldWidth - 1) / VoxelGridSize;
			int voxelCountY = (this.block.DensityFieldHeight - 1) / VoxelGridSize;
			int voxelCountZ = (this.block.DensityFieldDepth - 1) / VoxelGridSize;

			Vector3 minPtVoxelBoundaries = rawScaledPtMin / (float)VoxelGridSize;
			Vector3 maxPtVoxelBoundaries = rawScaledPtMax / (float)VoxelGridSize;

			int minVX = Mathf.Clamp((int)(minPtVoxelBoundaries.x)-1,0, voxelCountX-1);
			int minVY = Mathf.Clamp((int)(minPtVoxelBoundaries.y)-1,0, voxelCountY-1);
			int minVZ = Mathf.Clamp((int)(minPtVoxelBoundaries.z)-1,0, voxelCountZ-1);
			int maxVX = Mathf.Clamp((int)(maxPtVoxelBoundaries.x)+1,0, voxelCountX-1);
			int maxVY = Mathf.Clamp((int)(maxPtVoxelBoundaries.y)+1,0, voxelCountY-1);
			int maxVZ = Mathf.Clamp((int)(maxPtVoxelBoundaries.z)+1,0, voxelCountZ-1);
			*/

			/*
			int minVX = Mathf.Clamp(Mathf.FloorToInt(minPtVoxelBoundaries.x)-1,0, voxelCountX-1);
			int minVY = Mathf.Clamp(Mathf.FloorToInt(minPtVoxelBoundaries.y)-1,0, voxelCountY-1);
			int minVZ = Mathf.Clamp(Mathf.FloorToInt(minPtVoxelBoundaries.z)-1,0, voxelCountZ-1);
			int maxVX = Mathf.Clamp(Mathf.CeilToInt(maxPtVoxelBoundaries.x),0, voxelCountX-1);
			int maxVY = Mathf.Clamp(Mathf.CeilToInt(maxPtVoxelBoundaries.y),0, voxelCountY-1);
			int maxVZ = Mathf.Clamp(Mathf.CeilToInt(maxPtVoxelBoundaries.z),0, voxelCountZ-1);
			*/

			/*
			int minVX = Mathf.Clamp((int)((float)minX / (float)VoxelGridSize),0, voxelCountX-1);
			int minVY = Mathf.Clamp((int)((float)minY / (float)VoxelGridSize),0, voxelCountY-1);
			int minVZ = Mathf.Clamp((int)((float)minZ / (float)VoxelGridSize),0, voxelCountZ-1);
			int maxVX = Mathf.Clamp((int)((float)maxX / (float)VoxelGridSize),0, voxelCountX-1);
			int maxVY = Mathf.Clamp((int)((float)maxY / (float)VoxelGridSize),0, voxelCountY-1);
			int maxVZ = Mathf.Clamp((int)((float)maxZ / (float)VoxelGridSize),0, voxelCountZ-1);
			*/

			/*
			Debug.Log("X: " + minVX + " " + maxVX + " Y: " + minVY + " " + maxVY + " Z: " + minVZ + " " + maxVZ);
			Debug.Log("X: " + minPtVoxelBoundaries.x + " " + maxPtVoxelBoundaries.x + " Y: " + minPtVoxelBoundaries.y + " " + maxPtVoxelBoundaries.y + " Z: " + minPtVoxelBoundaries.z + " " + maxPtVoxelBoundaries.z);
			//Update voxel geometry
			for(int k = minVZ; k <= maxVZ; k++) {
				for(int j = minVY; j <= maxVY; j++) {
					for(int i = minVX; i <= maxVX; i++) {
						int stride = i + j*voxelCountX + k*voxelCountX*voxelCountY;
						voxels[stride].CreateGeometry();
					}
				}
			}
			*/
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.H)) {
			for(int i = 0; i < voxels.Length; i++) {
				voxels[i].CreateGeometry();
			}
		}
	}
}
