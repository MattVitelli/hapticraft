﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MarchingCubeBlock : MonoBehaviour {
	public TerrainGenerator parent;
	public int offsetX;
	public int offsetY;
	public int offsetZ;
	public int width;
	public int height;
	public int depth;
	public Bounds boundingBox;
	// Use this for initialization
	void Start () {

	}

	public void CreateGeometry() {
		CreateGeometry(null, 0, 0, 0);
	}

	public void CreateGeometry(TerrainManager mgr, int idX, int idY, int idZ) {
		Vector3 invScale = new Vector3(2.0f/(parent.block.DensityFieldWidth-1), 2.0f/(parent.block.DensityFieldHeight-1), 2.0f/(parent.block.DensityFieldDepth-1));

		Vector3 ptMin = new Vector3(offsetX, offsetY, offsetZ);
		Vector3 ptMax = new Vector3(width, height, depth)+ptMin;
		ptMin.Scale(invScale);
		ptMin -= Vector3.one;
		ptMax.Scale(invScale);
		ptMax -= Vector3.one;
		ptMin = this.parent.transform.TransformPoint(ptMin);
		ptMax = this.parent.transform.TransformPoint(ptMax);
		boundingBox.SetMinMax(ptMin, ptMax);

		SortedList<ulong, VoxelPrimitive> primitives = MarchingCubeHelper.GenerateGeometry(ref parent.block,
		                                                            parent.materialHandler,
		                                                            127,
		                                                            width,
		                                                            height,
		                                                            depth,
		                                                            offsetX,
		                                                            offsetY,
		                                                            offsetZ,
		                                                            mgr,
		                                                            idX,
		                                                            idY,
		                                                            idZ);

		for(int i = this.gameObject.transform.childCount-1; i >= 0; i--) {
			Transform t = this.gameObject.transform.GetChild(i);
			DestroyImmediate(t.gameObject);
		}

		this.gameObject.transform.DetachChildren();
		for(int i = 0; i < primitives.Keys.Count; i++) {
			VoxelPrimitive curPrim = primitives[primitives.Keys[i]];
			if(curPrim.vertices.Count > 0) {
				Bounds bounds = new Bounds();
				bounds.SetMinMax(curPrim.minBB, curPrim.maxBB);
				Mesh mesh = new Mesh();
				mesh.vertices = curPrim.vertices.ToArray();
				mesh.normals = curPrim.normals.ToArray();
				mesh.colors = curPrim.blendWeights.ToArray();
				mesh.tangents = new Vector4[curPrim.vertices.Count];
				mesh.triangles = curPrim.indices.ToArray();
				mesh.bounds = bounds;

				
				GameObject newPrefab = (GameObject)Instantiate(parent.emptyPrefab);
				MeshFilter meshFilter = newPrefab.GetComponent<MeshFilter>();
				MeshCollider collider = newPrefab.GetComponent<MeshCollider>();
				if(meshFilter == null) {
					meshFilter = newPrefab.AddComponent<MeshFilter>();
					newPrefab.AddComponent<MeshRenderer>();
				}
				if(collider == null) {
					collider = newPrefab.AddComponent<MeshCollider>();
				}
				meshFilter.mesh = mesh;
				meshFilter.renderer.material = parent.materialHandler.GetMaterial(curPrim.material);
				collider.convex = false;
				collider.sharedMesh = mesh;
				newPrefab.transform.parent = this.transform;
				newPrefab.transform.localScale = this.transform.localScale;
				newPrefab.transform.localPosition = Vector3.zero;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	}
}
