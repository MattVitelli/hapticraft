﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TerrainManager : MonoBehaviour {
	public int chunkSize = 200; //Defined in Unity units
	public int viewSize = 400; //Defined in Unity units
	public GameObject chunkPrefab;
	public Camera camera;
	public SortedList<string, TerrainGenerator> chunks = new SortedList<string, TerrainGenerator>();
	public TerrainMaterialHandler materialHandler;

	bool hasGeneratedChunks = false;

	public bool HasGeneratedSufficientChunks() {
		return hasGeneratedChunks;
	}

	// Use this for initialization
	void Start () {
		HandleChunkGeneration();
	}

	string formChunkName(int x, int y, int z) {
		return x + " " + y + " " + z;
	}

	void generateAroundRegion(int minX, int minY, int minZ,
	                          int maxX, int maxY, int maxZ) {
		for(int i = minX; i <= maxX; i++) {
			for(int j = minY; j <= maxY; j++) {
				for(int k = minZ; k <= maxZ; k++) {
					string curChunk = formChunkName(i, j, k);
					if(!chunks.ContainsKey(curChunk)) {
						GameObject obj = (GameObject)Instantiate(chunkPrefab);
						obj.name = curChunk;
						obj.transform.parent = this.transform;
						obj.transform.position = new Vector3(i*chunkSize, j*chunkSize, k*chunkSize)*2.0f;
						obj.transform.localScale = Vector3.one*(float)chunkSize;
						TerrainGenerator terrChunk = obj.GetComponent<TerrainGenerator>();
						terrChunk.idX = i;
						terrChunk.idY = j;
						terrChunk.idZ = k;
						terrChunk.materialHandler = this.materialHandler;
						chunks.Add(curChunk, terrChunk);
					}
				}
			}
		}
	}

	public TerrainGenerator[] GetNeighboringVoxels(TerrainGenerator gen) {
		List<TerrainGenerator> neighbors = new List<TerrainGenerator>();
		for(int i = -1; i <= 1; i++) {
			for(int j = -1; j <= 1; j++) {
				for(int k = -1; k <= 1; k++) {
					bool isCenterVoxel = (i==0 && j==0 && k==0);
					string chunkName = formChunkName(i+gen.idX, j+gen.idY, k+gen.idZ);
					if(!isCenterVoxel && chunks.ContainsKey(chunkName)) {
						neighbors.Add(chunks[chunkName]);
					}
				}
			}
		}
		return neighbors.ToArray();
	}

	void HandleChunkGeneration() {

		Vector3 camPos = camera.transform.position;
		int camX = (int)(camPos.x + (float)chunkSize*0.5f) / chunkSize;
		int camY = (int)(camPos.y + (float)chunkSize*0.5f) / chunkSize;
		int camZ = (int)(camPos.z + (float)chunkSize*0.5f) / chunkSize;
		int extent = 1;//viewSize / chunkSize;
		
		generateAroundRegion(camX-extent, camY-extent, camZ-extent,
		                     camX+extent, camY+extent, camZ+extent);

		if(chunks.Count > 1) {
			hasGeneratedChunks = true;
		}
	}
	
	// Update is called once per frame
	void Update () {


		//HandleChunkGeneration();

		/*
		for(int i = camX-extent; i <= camX+extent; i++) {
			for(int j = camY-extent; j <= camY+extent; j++) {
				for(int k = camZ-extent; k <= camZ+extent; k++) {
					string curChunk = i + " " + j + " " + k;
					if(!chunks.ContainsKey(curChunk)) {
						GameObject obj = (GameObject)Instantiate(chunkPrefab);
						obj.name = curChunk;
						obj.transform.parent = this.transform;
						obj.transform.position = new Vector3(i*chunkSize, j*chunkSize, k*chunkSize)*2.0f;
						obj.transform.localScale = Vector3.one*(float)chunkSize;
						TerrainGenerator terrChunk = obj.GetComponent<TerrainGenerator>();
						terrChunk.materialHandler = this.materialHandler;
						chunks.Add(curChunk, terrChunk);
					}
				}
			}
		}
		*/

	}
}
