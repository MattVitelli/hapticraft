﻿using UnityEngine;
using System.Collections;

public static class ProceduralTerrainHelper {

	static float[] densities = {0.007f, 0.05f, 0.3f, 0.8f};
	static float[] amplitudes = {0.94f, 0.24f, 0.13f, 0.1f};
	static float[] heightAmplitudes = {0.3f, 0.5f, 1.2f, 1.5f};
	const float FLOOR = 4.0f/255.0f;
	const float MIN_Y = -256;
	const float MAX_Y = 768;
	const int SMOOTH_RADIUS = 1;
	//Takes in a world-space point, returns a density
	//Expects a point delta (i.e. WorldScale/TextureResolution) to be passed to it
	public static float GetDensityAtPoint(Vector3 point, Vector3 deltaPt) {
		float density = 0;
		float weight = 0;
		for(int i = -SMOOTH_RADIUS; i <=SMOOTH_RADIUS; i++) {
			for(int j = -SMOOTH_RADIUS; j<=SMOOTH_RADIUS; j++) {
				for(int k = -SMOOTH_RADIUS; k<=SMOOTH_RADIUS; k++) {
					Vector3 wp = new Vector3(i,j,k);
					wp.Scale(deltaPt);
					wp += point;
					//Convert point's height into 0-1 range
					float y = (wp.y-MIN_Y) / (MAX_Y-MIN_Y);
					float height = GetHeightmapAtPoint(wp);
					float ratio = y / Mathf.Max(height, FLOOR);
					density += Mathf.Clamp01(1.0f-ratio);
					weight++;
				}
			}
		}
		return density / weight;
	}

	public static void SmoothDensityField(ref float[] densityField, VoxelBlock block, Vector3 deltaPt, Vector3 scale, Vector3 offset) {
		for(int z = 0; z < block.DensityFieldDepth; z++) {
			for(int y = 0; y < block.DensityFieldHeight; y++) {
				for(int x = 0; x < block.DensityFieldWidth; x++) {
					int stride = x + y * block.DensityFieldWidth + z * block.DensityFieldWidth*block.DensityFieldHeight;

					float density = 0.0f;
					float weight = 0.0f;
					for(int k = z-SMOOTH_RADIUS; k <=z+SMOOTH_RADIUS; k++) {
						for(int j = y-SMOOTH_RADIUS; j<=y+SMOOTH_RADIUS; j++) {
							for(int i = x-SMOOTH_RADIUS; i<=x+SMOOTH_RADIUS; i++) {
								//If its not in our array, sample it from function
								if(i < 0 || i >= block.DensityFieldWidth || j < 0 || j >= block.DensityFieldHeight || k < 0 || k >= block.DensityFieldDepth ) {
									Vector3 wp = new Vector3(i,j,k);
									wp.Scale(scale);
									wp += offset;
									density += GetDensityAtPoint(wp);
								}
								//Otherwise, sample it from array
								else {
									int sampStride = i + j * block.DensityFieldWidth + k * block.DensityFieldWidth*block.DensityFieldHeight;
									density += densityField[sampStride];
								}
								weight++;
							}
						}
					}

					block.DensityField[stride] = (byte)(255.0f*density/weight);

				}
			}
		}
	}

	public static float GetDensityAtPoint(Vector3 wp) {
		float density = 0.0f;
		float y = (wp.y-MIN_Y) / (MAX_Y-MIN_Y);
		float height = GetHeightmapAtPoint(wp);
		float ratio = y / Mathf.Max(height, FLOOR);
		density += Mathf.Clamp01(1.0f-ratio);
		return density;
	}

	static float GetHeightmapAtPoint(Vector3 point) {
		float density = 0;

		for(int i = 0; i < densities.Length; i++) {
			Vector2 pt = new Vector2(point.x, point.z);
			pt *= densities[i];
			density += Mathf.PerlinNoise(pt.x, pt.y)*amplitudes[i];
			pt += Vector2.one*Mathf.Cos(point.y)*heightAmplitudes[i];
		}
		density /= (float)densities.Length;
		return density;
	}
}
