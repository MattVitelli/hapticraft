﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class TerrainMaterialHandler : MonoBehaviour {

	public Texture2D[] BaseMaps;
	public Texture2D[] BumpMaps;
	public Material TerrainDefaultMaterial;

	SortedList<ulong, Material> materials = new SortedList<ulong, Material>();
	public Material GetMaterial(MaterialPrimitive primitive) {
		ulong key = primitive.GetID();
		if(!materials.ContainsKey(key)) {
			Material mat = new Material(TerrainDefaultMaterial);
			mat.SetTexture("_BaseTex0", BaseMaps[primitive.id0]);
			mat.SetTexture("_BumpTex0", BumpMaps[primitive.id0]);
			mat.SetTexture("_BaseTex1", BaseMaps[primitive.id1]);
			mat.SetTexture("_BumpTex1", BumpMaps[primitive.id1]);
			mat.SetTexture("_BaseTex2", BaseMaps[primitive.id2]);
			mat.SetTexture("_BumpTex2", BumpMaps[primitive.id2]);
			mat.SetTexture("_BaseTex3", BaseMaps[primitive.id3]);
			mat.SetTexture("_BumpTex3", BumpMaps[primitive.id3]);
			Debug.Log("Created new material " + primitive.id0 + " " + primitive.id1 + " " + primitive.id2 + " " + primitive.id3);
			materials.Add(key, mat);
		}
		return materials[key];
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
