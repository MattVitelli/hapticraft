﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VoxelBlock {
	public byte[] DensityField;
	public byte[] MaterialField;
	public int DensityFieldWidth;
	public int DensityFieldHeight;
	public int DensityFieldDepth;
	public bool IsAllocated;
	public VoxelBlock(int width, int height, int depth) {
		IsAllocated = true;
		DensityField = new byte[width*height*depth];
		MaterialField = new byte[width*height*depth];
		DensityFieldWidth = width;
		DensityFieldHeight = height;
		DensityFieldDepth = depth;
	}
}

public struct MaterialPrimitive {
	public int id0;
	public int id1;
	public int id2;
	public int id3;

	public MaterialPrimitive(int id0, int id1, int id2, int id3) {
		this.id0 = id0;
		this.id1 = id1;
		this.id2 = id2;
		this.id3 = id3;
	}

	public ulong GetID() {
		ulong id = 0;
		id |= (ulong)id0;
		id |= ((ulong)id1 << 8);
		id |= ((ulong)id2 << 16);
		id |= ((ulong)id3 << 24);
		return id;
	}
}

public class VoxelPrimitive {
	public List<Vector3> vertices;
	public List<Vector3> normals;
	public List<Color> blendWeights;
	public List<int> indices;
	public SortedList<int, int> edgeToIndices;
	public MaterialPrimitive material;
	
	public Vector3 minBB;
	public Vector3 maxBB;

	public VoxelPrimitive()
	{
		vertices = new List<Vector3>();
		normals = new List<Vector3>();
		blendWeights = new List<Color>();
		indices = new List<int>();
		edgeToIndices = new SortedList<int, int>();
		minBB = Vector3.one*float.MaxValue;
		maxBB = -Vector3.one*float.MaxValue;
	}
}
