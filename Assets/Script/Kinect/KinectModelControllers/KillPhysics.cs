﻿using UnityEngine;
using System.Collections;

public class KillPhysics : MonoBehaviour {
	public bool enabled = true;
	// Use this for initialization
	void Start () {
		if(enabled) {
			Rigidbody[] bodies = this.GetComponentsInChildren<Rigidbody>();
			foreach(Rigidbody b in bodies) {
				b.isKinematic = true;
			}
			Collider[] colls = this.GetComponentsInChildren<Collider>();
			foreach(Collider c in colls) {
				c.enabled = false;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	}
}
