﻿using UnityEngine;
using System.Collections;

public class KinectBoneRotationCorrector : MonoBehaviour {
	public Transform boneToRotate;
	Quaternion sourceRotation;
	Quaternion relativeRotation;
	// Use this for initialization
	void Start () {

		sourceRotation = boneToRotate.localRotation;
		Quaternion origRotation = this.transform.localRotation;
		/*
		sourceRotation = boneToRotate.rotation;
		origRotation = this.transform.rotation;
		*/
		relativeRotation = Quaternion.Inverse(origRotation) * sourceRotation;
		relativeRotation = Quaternion.Inverse(sourceRotation) * origRotation;
	}
	
	// Update is called once per frame
	void Update () {
		boneToRotate.localRotation = Quaternion.Inverse(this.transform.localRotation) * relativeRotation;
		boneToRotate.localRotation = relativeRotation*Quaternion.Inverse(this.transform.localRotation);
	}
}
