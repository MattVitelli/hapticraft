﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KinectModelRecorder : MonoBehaviour {
	public bool isRecording = false;
	public KinectModelControllerV2 model;
	public string outputDirectory;
	public string clipName;
	public AnimationClip sampleClip;
	public Transform root;

	const float KINECT_FRAME_RATE = 30.0f;
	const float ROTATION_EPSILON = 3.0e-3f;
	const float TRANSLATION_EPSILON = 1.0e-3f;

	const string LOCAL_POSITION_X = "m_LocalPosition.x";
	const string LOCAL_POSITION_Y = "m_LocalPosition.y";
	const string LOCAL_POSITION_Z = "m_LocalPosition.z";
	const string LOCAL_ROTATION_X = "m_LocalRotation.x";
	const string LOCAL_ROTATION_Y = "m_LocalRotation.y";
	const string LOCAL_ROTATION_Z = "m_LocalRotation.z";
	const string LOCAL_ROTATION_W = "m_LocalRotation.w";


	/*
	const string LOCAL_POSITION_X = "localPosition.x";
	const string LOCAL_POSITION_Y = "localPosition.y";
	const string LOCAL_POSITION_Z = "localPosition.z";
	const string LOCAL_ROTATION_X = "localRotation.x";
	const string LOCAL_ROTATION_Y = "localRotation.y";
	const string LOCAL_ROTATION_Z = "localRotation.z";
	const string LOCAL_ROTATION_W = "localRotation.w";
	*/
	bool wasRecording = false;
	float elapsedRecordingTime = 0.0f;

	Transform[] bones;
	public AnimationClip recordedClip;
	SortedList<int, string> transformsToNames;
	SortedList<int, AnimationCurveHelper> transformsToCurves;

	// Use this for initialization
	void Start () {
		/*
		AnimationClipCurveData[] AnimClipCurveData = AnimationUtility.GetAllCurves( sampleClip, true );
		foreach( AnimationClipCurveData curve in AnimClipCurveData )
		{
			Debug.Log(curve.path);
			Debug.Log("Curve Type = " + curve.type.ToString());
			Debug.Log("Curve property = " + curve.propertyName);
			//AnimationUtility.SetEditorCurve( _AnimClip, curve.path, curve.type, curve.propertyName, curve.curve );
		}
		*/
		buildNameTree();
	}
	
	// Update is called once per frame
	void Update () {

		handleKeyboardInput();
		updateRecording();
	}

	void handleKeyboardInput() {
		wasRecording = isRecording;
		if(Input.GetKeyUp(KeyCode.R)) {
			isRecording = !isRecording;
		}
		if(!wasRecording && isRecording) {
			startRecording();
		}
		else if(wasRecording && !isRecording) {
			endRecording();
		}
	}

	void updateRecording() {
		if(!isRecording) {
			return;
		}
		float dt = Time.deltaTime;
		for(int i = 0; i <  bones.Length; i++) {
			transformsToCurves[i].UpdateAnimation(bones[i], elapsedRecordingTime, dt);
		}
		elapsedRecordingTime += dt;// * recordedClip.frameRate;
	}

	void startRecording() {
		isRecording = true;
		elapsedRecordingTime = 0.0f;
		string outputFileName = outputDirectory + clipName + ".anim";
		Debug.Log("Recording to " + outputFileName);
		createAnimations();
	}

	void endRecording() {
		isRecording = false;
		for(int i = 0; i < bones.Length; i++) {
			string boneName = transformsToNames[i];
			transformsToCurves[i].SaveClip(boneName, recordedClip);
		}
		/*
		if(!recordedClip.ValidateIfRetargetable(true)) {
			Debug.Log("Problem validating clip!");
		}
		string outputFileName = outputDirectory + clipName + ".anim";

		string outputPath = AssetDatabase.GenerateUniqueAssetPath(outputFileName);

		AssetDatabase.CreateAsset(recordedClip,outputPath);
		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();

		Debug.Log(AssetDatabase.GetAssetPath(recordedClip));
		Debug.Log("Finished writing to " + outputPath);
		*/
	}


	void buildNameTree() {
		/*
		transformsToNames = new SortedList<int, string>();
		bones = model.GetBones();
		for(int i = 0; i < bones.Length; i++) {
			string transformName = AnimationUtility.CalculateTransformPath(bones[i], root);
			transformsToNames.Add(i, transformName);
		}
		*/
	}

	public struct AnimationCurveHelper {
		AnimationCurve curve_x;
		AnimationCurve curve_y;
		AnimationCurve curve_z;
		AnimationCurve curve_qx;
		AnimationCurve curve_qy;
		AnimationCurve curve_qz;
		AnimationCurve curve_qw;
		Vector3 lastTDeriv;
		Vector4 lastRDeriv;
		public AnimationCurveHelper(string name, AnimationClip clip) {
			curve_x = new AnimationCurve();
			curve_y = new AnimationCurve();
			curve_z = new AnimationCurve();
			curve_qx = new AnimationCurve();
			curve_qy = new AnimationCurve();
			curve_qz = new AnimationCurve();
			curve_qw = new AnimationCurve();
			lastTDeriv = Vector3.zero;
			lastRDeriv = Vector4.zero;

			clip.SetCurve(name, typeof(Transform), LOCAL_POSITION_X, curve_x);
			clip.SetCurve(name, typeof(Transform), LOCAL_POSITION_Y, curve_y);
			clip.SetCurve(name, typeof(Transform), LOCAL_POSITION_Z, curve_z);
			clip.SetCurve(name, typeof(Transform), LOCAL_ROTATION_X, curve_qx);
			clip.SetCurve(name, typeof(Transform), LOCAL_ROTATION_Y, curve_qy);
			clip.SetCurve(name, typeof(Transform), LOCAL_ROTATION_Z, curve_qz);
			clip.SetCurve(name, typeof(Transform), LOCAL_ROTATION_W, curve_qw);
		}

		public void SaveClip(string name, AnimationClip clip) {
			clip.SetCurve(name, typeof(Transform), LOCAL_POSITION_X, curve_x);
			clip.SetCurve(name, typeof(Transform), LOCAL_POSITION_Y, curve_y);
			clip.SetCurve(name, typeof(Transform), LOCAL_POSITION_Z, curve_z);
			clip.SetCurve(name, typeof(Transform), LOCAL_ROTATION_X, curve_qx);
			clip.SetCurve(name, typeof(Transform), LOCAL_ROTATION_Y, curve_qy);
			clip.SetCurve(name, typeof(Transform), LOCAL_ROTATION_Z, curve_qz);
			clip.SetCurve(name, typeof(Transform), LOCAL_ROTATION_W, curve_qw);
		}

		Keyframe createKeyframe(Transform t, float time, int indexType) {
			float value = 0.0f;
			switch(indexType) {
			case 0:
				value = t.localPosition.x;
				break;
			case 1:
				value = t.localPosition.y;
				break;
			case 2:
				value = t.localPosition.z;
				break;
			case 3:
				value = t.localRotation.x;
				break;
			case 4:
				value = t.localRotation.y;
				break;
			case 5:
				value = t.localRotation.z;
				break;
			case 6:
				value = t.localRotation.w;
				break;
			}
			Keyframe frame = new Keyframe(time, value);
			return frame;
		}

		float getLastKey(int indexType) {
			float tDummy;
			bool bDummy;
			return getLastKey(indexType, out tDummy, out bDummy);
		}

		float getLastKey(int indexType, out float lastKeyTime, out bool noKeyframes) {
			AnimationCurve curCurve = null;
			switch(indexType) {
			case 0:
				curCurve = curve_x;
				break;
			case 1:
				curCurve = curve_y;
				break;
			case 2:
				curCurve = curve_z;
				break;
			case 3:
				curCurve = curve_qx;
				break;
			case 4:
				curCurve = curve_qy;
				break;
			case 5:
				curCurve = curve_qz;
				break;
			case 6:
				curCurve = curve_qw;
				break;
			}
			if(curCurve.keys != null && curCurve.keys.Length > 0) {
				lastKeyTime = curCurve.keys[curCurve.keys.Length-1].time;
				noKeyframes = false;
				return curCurve.keys[curCurve.keys.Length-1].value;
			}
			else {
				lastKeyTime = -0.0001f;
				noKeyframes = true;
				return 0;
			}
		}

		void getPreviousRT(out Vector3 prevT, out Vector4 prevR) {

			prevT = new Vector3(getLastKey(0), getLastKey(1), getLastKey(2));
			prevR = new Vector4(getLastKey(3), getLastKey(4), getLastKey(5), getLastKey(6));
		}

		bool derivativeChanged(Transform t, int indexType, float time) {
			bool changed = false;
			float lastD = 0;
			float curV = 0;
			float eps = TRANSLATION_EPSILON;
			switch(indexType) {
			case 0:
				lastD = lastTDeriv.x;
				curV = t.localPosition.x;
				eps = TRANSLATION_EPSILON;
				break;
			case 1:
				lastD = lastTDeriv.y;
				curV = t.localPosition.y;
				eps = TRANSLATION_EPSILON;
				break;
			case 2:
				lastD = lastTDeriv.z;
				curV = t.localPosition.z;
				eps = TRANSLATION_EPSILON;
				break;
			case 3:
				lastD = lastRDeriv.x;
				curV = t.localRotation.x;
				eps = ROTATION_EPSILON;
				break;
			case 4:
				lastD = lastRDeriv.y;
				curV = t.localRotation.y;
				eps = ROTATION_EPSILON;
				break;
			case 5:
				lastD = lastRDeriv.z;
				curV = t.localRotation.z;
				eps = ROTATION_EPSILON;
				break;
			case 6:
				lastD = lastRDeriv.w;
				curV = t.localRotation.w;
				eps = ROTATION_EPSILON;
				break;
			}
			float lastTime;
			bool noKeyframes = false;
			float lastV = getLastKey(indexType, out lastTime, out noKeyframes);
			float dt = time - lastTime;
			float dv = curV - lastV;
			float dvdt = dv/dt;
			switch(indexType) {
			case 0:
				lastTDeriv.x = dvdt;
				break;
			case 1:
				lastTDeriv.y = dvdt;
				break;
			case 2:
				lastTDeriv.z = dvdt;
				break;
			case 3:
				lastRDeriv.x = dvdt;
				break;
			case 4:
				lastRDeriv.y = dvdt;
				break;
			case 5:
				lastRDeriv.z = dvdt;
				break;
			case 6:
				lastRDeriv.w = dvdt;
				break;
			}
			return (Mathf.Abs(dvdt) > eps || noKeyframes);
		}

		//TODO - Derivative-based keyframe adding
		public void UpdateAnimation(Transform transform, float time, float dt) {

			/*
			Vector3 prevT;
			Vector4 prevR;
			getPreviousRT(out prevT, out prevR);
			if((transform.localPosition-prevT).magnitude > TRANSLATION_EPSILON) {
				curve_x.AddKey(createKeyframe(transform, time, 0));
				curve_y.AddKey(createKeyframe(transform, time, 1));
				curve_z.AddKey(createKeyframe(transform, time, 2));
			}
			Vector4 r = new Vector4(transform.localRotation.x,
			                        transform.localRotation.y,
			                        transform.localRotation.z,
			                        transform.localRotation.w);
			if((r-prevR).magnitude > ROTATION_EPSILON) {
				curve_qx.AddKey(createKeyframe(transform, time, 3));
				curve_qy.AddKey(createKeyframe(transform, time, 4));
				curve_qz.AddKey(createKeyframe(transform, time, 5));
				curve_qw.AddKey(createKeyframe(transform, time, 6));
			}
			*/
			if(derivativeChanged(transform, 0, time)) {
				curve_x.AddKey(createKeyframe(transform, time, 0));
			}
			if(derivativeChanged(transform, 1, time)) {
				curve_y.AddKey(createKeyframe(transform, time, 1));
			}
			if(derivativeChanged(transform, 2, time)) {
				curve_z.AddKey(createKeyframe(transform, time, 2));
			}
			if(derivativeChanged(transform, 3, time)) {
				curve_qx.AddKey(createKeyframe(transform, time, 3));
			}
			if(derivativeChanged(transform, 4, time)) {
				curve_qy.AddKey(createKeyframe(transform, time, 4));
			}
			if(derivativeChanged(transform, 5, time)) {
				curve_qz.AddKey(createKeyframe(transform, time, 5));
			}
			if(derivativeChanged(transform, 6, time)) {
				curve_qw.AddKey(createKeyframe(transform, time, 6));
			}
		}
	};

	void createAnimations() {
		/*
		transformsToCurves = new SortedList<int, AnimationCurveHelper>();
		recordedClip = new AnimationClip();
		EditorUtility.CopySerialized(sampleClip, recordedClip);
		recordedClip.ClearCurves();
		recordedClip.name = clipName;
		recordedClip.frameRate = KINECT_FRAME_RATE;
		int numBones = bones.Length;
		for(int i = 0; i < numBones; i++) {
			AnimationCurveHelper helper = new AnimationCurveHelper(transformsToNames[i], recordedClip);
			transformsToCurves.Add(i, helper);
		}
		*/
	}
}
